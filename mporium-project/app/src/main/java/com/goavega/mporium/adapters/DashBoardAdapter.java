package com.goavega.mporium.adapters;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.goavega.mporium.Activities.DashboardDisplayActivity;
import com.goavega.mporium.Model.DashBoardModel;
import com.goavega.mporium.R;

import java.util.ArrayList;

/**
 * Created by RAKESH on 12/9/2015.
 */
public class DashBoardAdapter extends BaseAdapter {

    private LayoutInflater layoutInflater;
    Context context;
    public ArrayList<DashBoardModel> dashBoardList= new ArrayList<DashBoardModel>();

    public DashBoardAdapter(DashboardDisplayActivity activity, ArrayList<DashBoardModel> dashBoardList) {
        layoutInflater=(LayoutInflater)activity
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
       this.dashBoardList=dashBoardList;
        this.context=activity;
        }

    @Override
    public int getCount() {
        return this.dashBoardList.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View listItem = convertView;
        int pos = position;
        TextView deviceNameText,
                manufacturerText,
                purchaseDateText,
                serviceScheduleDateText,
                remainingDaysText,
                warrantyDateText,
                warrantyDaysLeftText;
        LinearLayout indicatorText;
        ImageView reviewImage;
        if (listItem == null) {
            listItem = layoutInflater.inflate(R.layout.dashboard_display, null);
            listItem.setBackgroundColor(Color.WHITE);
            LinearLayout titleBackground=(LinearLayout)listItem.findViewById(R.id.mainTitle);
            int color=pos%3;
            if(color==1)
                titleBackground.setBackgroundResource(R.color.pink);
            if(color==2)
                titleBackground.setBackgroundResource(R.color.lightGreens);
            if(color==3)
                titleBackground.setBackgroundResource(R.color.lightBlack);

            deviceNameText = (TextView) listItem.findViewById(R.id.deviceNameText);
            manufacturerText=(TextView)listItem.findViewById(R.id.manufacturerNameText);
            purchaseDateText=(TextView)listItem.findViewById(R.id.purchaseDateTexts);
            serviceScheduleDateText=(TextView)listItem.findViewById(R.id.serviceScheduleDateText);
            remainingDaysText=(TextView)listItem.findViewById(R.id.daysLeftText);
            warrantyDateText=(TextView)listItem.findViewById(R.id.warrantyDateText);
            warrantyDaysLeftText=(TextView)listItem.findViewById(R.id.warrantyDaysLeftText);
            indicatorText=(LinearLayout)listItem.findViewById(R.id.expiredColor);
            deviceNameText.setText(this.dashBoardList.get(position).getDeviceName());
            manufacturerText.setText(this.dashBoardList.get(position).getManufacturerName());
            purchaseDateText.setText(this.dashBoardList.get(position).getPurchaseDate());
            reviewImage=(ImageView)listItem.findViewById(R.id.reviewService);

            int servicePeriod=Integer.parseInt(this.dashBoardList.get(position).getServiceSchedulePeriod().toString());
            if(servicePeriod==0) {
                serviceScheduleDateText.setText("Not Set");
            }   else
                serviceScheduleDateText.setText(this.dashBoardList.get(position).getServiceScheduleDate());
            int warrantyPeriod=this.dashBoardList.get(position).getWarrantyPeriods();
            if(warrantyPeriod==0)
                warrantyDateText.setText("Not Set");
            else
                warrantyDateText.setText(this.dashBoardList.get(position).getWarrantyPeriodExpire());
            if(this.dashBoardList.get(position).getServiceRemainingDays()>0)
                remainingDaysText.setText(this.dashBoardList.get(position).getServiceRemainingDays().toString()+"Days Left");
            if(this.dashBoardList.get(position).getServiceRemainingDays()<=0&&servicePeriod!=0)
            {
                remainingDaysText.setText("Service Expired");
                reviewImage.setVisibility(View.VISIBLE);
            }
            remainingDaysText.setTextColor(Color.parseColor(this.dashBoardList.get(position).getColor()));
            indicatorText.setBackgroundColor(Color.parseColor(this.dashBoardList.get(position).getColor()));

            if(this.dashBoardList.get(position).getWarrantyRemainingDays()>0)
                warrantyDaysLeftText.setText(this.dashBoardList.get(position).getWarrantyRemainingDays().toString()+"Days Left");
            if(this.dashBoardList.get(position).getWarrantyRemainingDays()<=0&&warrantyPeriod!=0)
                warrantyDaysLeftText.setText("warranty Expired");
        }
        return listItem;
    }
}
