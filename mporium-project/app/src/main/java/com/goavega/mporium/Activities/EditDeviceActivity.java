package com.goavega.mporium.Activities;

import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
import com.goavega.mporium.ApiDatas.ApiHelper;
import com.goavega.mporium.ApiDatas.ApiUrls;
import com.goavega.mporium.Classes.SaveUserLogin;
import com.goavega.mporium.Model.DashBoardModel;
import com.goavega.mporium.Model.DeviceDetailModel;
import com.goavega.mporium.Model.ServiceProviderModel;
import com.goavega.mporium.R;
import com.goavega.mporium.Services.DeviceService;
import com.goavega.mporium.Services.ServiceProviderService;
import com.goavega.mporium.db.AddDeviceTable;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class EditDeviceActivity extends AppCompatActivity {
    TextView deviceNameText,
            manufacturerNameText,
            purchaseDateTexts,
            modelNumberText,
            serialNumberText;
    TextView serviceScheduleText;

    EditText
            warrantyText,
            serviceProviderNameText;
    AddDeviceTable addDeviceTable;
    Long serviceProviderIdFromText,
            serviceProviderId,
            updateServiceProviderID;
    Boolean updated=false;
    ApiHelper apiHelper;
    ApiUrls apiUrl;
    Long deviceId;
    DashBoardModel dashBoardModel;
    Button deviceServiceButton;
    DeviceService deviceService;
    String serviceSchedulePeriod,
            warrantyPeriod,
        serviceProviderName,
    serviceProviderCity,
    serviceProviderPhone;
    DeviceDetailModel deviceDetailModel;
    ImageView backArrow;
    TextView cancelText;

    TextView custom_message;
    Toast toast;
    View layout;

    ArrayList<Long> serviceProviderIdArray=new ArrayList<Long>();
    ArrayList<String> serviceProviderNameArray=new ArrayList<String>();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_device);
        cancelText=(TextView)findViewById(R.id.cancelText);
        deviceNameText=(TextView)findViewById(R.id.deviceNameText);
        manufacturerNameText=(TextView)findViewById(R.id.manufacturerNameText);
        purchaseDateTexts=(TextView)findViewById(R.id.purchaseDateText);
        modelNumberText=(TextView)findViewById(R.id.modelNumberTexts);
        serialNumberText=(TextView)findViewById(R.id.serialNumberText);
        serviceProviderNameText=(EditText)findViewById(R.id.serviceProviderText);
        serviceScheduleText=(EditText)findViewById(R.id.servicescheduleText);
        warrantyText=(EditText)findViewById(R.id.warrantyText);
        backArrow=(ImageView)findViewById(R.id.backArrowImage);
        deviceService=new DeviceService(getApplicationContext());
        deviceServiceButton=(Button)findViewById(R.id.addButton);
        apiHelper=new ApiHelper(getApplicationContext());
        apiUrl = new ApiUrls();
        deviceDetailModel=new DeviceDetailModel();
        addDeviceTable=new AddDeviceTable(getApplicationContext());
        dashBoardModel=new DashBoardModel();
        Intent calledIntent = getIntent();
        Bundle extras = calledIntent.getExtras();
        if(extras != null) {
            deviceId = extras.getLong("deviceId");
         }

        LayoutInflater li = getLayoutInflater();
        layout = li.inflate(R.layout.success_message_display,
                (ViewGroup) findViewById(R.id.message_display));
        toast = new Toast(getApplicationContext());
        toast.setDuration(Toast.LENGTH_SHORT);
        toast.setGravity(Gravity.FILL_HORIZONTAL | Gravity.TOP, 0, 0);
        toast.setView(layout);
        custom_message=(TextView)layout.findViewById(R.id.messageText);

        deviceDetailModel.setDeviceId(deviceId);
        deviceDetailModel=deviceService.setDeviceDetails(deviceDetailModel);
        serviceProviderCity=deviceDetailModel.getServiceProviderCity().toString();
        serviceProviderPhone=deviceDetailModel.getServviceProviderPhoneNumber().toString();
        updateServiceProviderID=deviceDetailModel.getServiceProviderId().longValue();
        manufacturerNameText.setText(deviceDetailModel.getManufacturerName());
        serviceProviderNameText.setText(deviceDetailModel.getServiceProviderName());
        modelNumberText.setText(deviceDetailModel.getModelNumber());
        serialNumberText.setText(deviceDetailModel.getSerialNumber());
        int frequencyPeriod=deviceDetailModel.getFrequencyService();
        if(frequencyPeriod==0)
            serviceScheduleText.setText(deviceDetailModel.getServiceSchedulePeriod().toString());
        else
            serviceScheduleText.setText(deviceDetailModel.getFrequencyService().toString());
        deviceNameText.setText(deviceDetailModel.getDeviceName().toString());
        purchaseDateTexts.setText(deviceDetailModel.getPurchaseDate());
        warrantyText.setText(deviceDetailModel.getWarrantyPeriod().toString());
        deviceServiceButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                boolean isUpdated;
                serviceSchedulePeriod = serviceScheduleText.getText().toString();
                warrantyPeriod = warrantyText.getText().toString();
                serviceProviderName=serviceProviderNameText.getText().toString();
                if(!serviceSchedulePeriod.isEmpty())
                deviceDetailModel.setServiceSchedulePeriod(Integer.parseInt(serviceSchedulePeriod));
                if(!warrantyPeriod.isEmpty())
                deviceDetailModel.setWarrantyPeriod(Integer.parseInt(warrantyPeriod));
                deviceDetailModel.setServiceProviderName(serviceProviderName);
                deviceDetailModel.setDeviceId(deviceId);
                isUpdated=deviceService.deviceUpdate(deviceDetailModel);
                if(isUpdated)
                {
                    layout.setBackgroundResource(R.color.lightGreen);
                    custom_message.setText(R.string.device_update);
                    toast.show();

                    //Toast.makeText(getApplicationContext(), "Device Details updated", Toast.LENGTH_LONG).show();
                    Intent gotoDashBoard = new Intent(getApplicationContext(), DashboardDisplayActivity.class);
                    startActivity(gotoDashBoard);
                    finish();

                }
                else {
                    layout.setBackgroundResource(R.color.lightGreen);
                    custom_message.setText(R.string.not_set_warranty);
                    toast.show();

                    //Toast.makeText(getApplicationContext(), "You are not edited warranty period or service period", Toast.LENGTH_LONG).show();
                }
            }
        });

        serviceProviderNameText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                addServiceProvider();
            }
        });

        cancelText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        backArrow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent gotoDashBoard=new Intent(getApplicationContext(),DashboardDisplayActivity.class);
                startActivity(gotoDashBoard);
            }
        });
    }

    public void addServiceProvider()
    {
        final EditText
                mobilNumberText;
        final AutoCompleteTextView nameText,cityText;
        TextView cancelText,
                 addServiceProviderText;
        deviceDetailModel.setDeviceId(deviceId);
        deviceDetailModel=deviceService.setDeviceDetails(deviceDetailModel);
        serviceProviderCity=deviceDetailModel.getServiceProviderCity().toString();
        serviceProviderPhone=deviceDetailModel.getServviceProviderPhoneNumber().toString();
        updateServiceProviderID=deviceDetailModel.getServiceProviderId().longValue();

        callServiceProvider();
        Button titleText;
        final Dialog d = new Dialog(EditDeviceActivity.this);
        d.setTitle("");
        d.requestWindowFeature(Window.FEATURE_NO_TITLE);
        d.setContentView(R.layout.add_service_provider_dialog);
        titleText=(Button)d.findViewById(R.id.titleText);
        titleText.setText("Update Service Provider");
        cancelText = (TextView) d.findViewById(R.id.cancelText);
        addServiceProviderText = (TextView) d.findViewById(R.id.addManufacturerText);
        addServiceProviderText.setText("Update");
        nameText=(AutoCompleteTextView)d.findViewById(R.id.NameText);
        cityText=(AutoCompleteTextView)d.findViewById(R.id.cityText);
        mobilNumberText=(EditText)d.findViewById(R.id.mobileNumberText);
        ArrayAdapter<String> serviceProviderAdapter=new ArrayAdapter(this,android.R.layout.simple_list_item_1,serviceProviderNameArray);
        nameText.setAdapter(serviceProviderAdapter);
        ArrayAdapter serviceProviderCityAdapter=new ArrayAdapter(this,android.R.layout.simple_list_item_1,getResources().getStringArray(R.array.cities));
        cityText.setAdapter(serviceProviderCityAdapter);

        nameText.setText(serviceProviderNameText.getText().toString());
        cityText.setText(serviceProviderCity);
        mobilNumberText.setText(serviceProviderPhone);

        d.show();
        nameText.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                serviceProviderIdFromText = serviceProviderIdArray.get(position);

            }
        });

        addServiceProviderText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int f=0,g=0;

                Boolean isSaved = false;
                ServiceProviderModel serviceProviderModel;
                ServiceProviderService serviceProviderService;
                serviceProviderModel = new ServiceProviderModel();
                serviceProviderService = new ServiceProviderService(getApplicationContext());
                serviceProviderModel.setName(nameText.getText().toString().toUpperCase());
                serviceProviderModel.setCity(cityText.getText().toString());
                serviceProviderModel.setPhoneNumber(mobilNumberText.getText().toString());
                serviceProviderModel.setServiceProviderId(updateServiceProviderID);

                if(nameText.getText().toString().isEmpty()) {
                    nameText.setHint("Name cannot be blank");
                    nameText.setHintTextColor(getResources().getColor(R.color.red));
                    f=1;
                }
                else
                    serviceProviderModel.setName(nameText.getText().toString().toUpperCase());
                int length=mobilNumberText.getText().length();

                if(!mobilNumberText.getText().toString().isEmpty()) {

                    if (length<10)
                    {
                        mobilNumberText.setText("");
                        mobilNumberText.setHint("mobile number should be 10 digit");
                        mobilNumberText.setHintTextColor(getResources().getColor(R.color.red));
                        g=1;
                    }
                }
                else
                    serviceProviderModel.setPhoneNumber(mobilNumberText.getText().toString());

                isSaved = serviceProviderService.sendsServiceProviderData(serviceProviderModel);
                if (isSaved) {

                    serviceProviderNameText.setText(serviceProviderModel.getName());
                    callServiceProvider();
                    d.dismiss();
                }
                if(f==0&&g==0)
                {
                    serviceProviderNameText.setText(serviceProviderModel.getName());
                    d.dismiss();
                    //Toast.makeText(getApplicationContext(),"Service Provider Details Updated",Toast.LENGTH_LONG).show();
                }

            }

        });

        cancelText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                d.dismiss();
            }
        });
    }

    public void callServiceProvider() {
        JSONArray serviceProviderJsonArray=null;
        JSONObject serviceProviderObject=null;
        try {
            String serviceProviderDetails =apiHelper.readAllUserDetails(getString(R.string.url)+apiUrl.getServiceProviderUrl);
            serviceProviderObject = new JSONObject(serviceProviderDetails);
            serviceProviderJsonArray = serviceProviderObject.getJSONArray("serviceProviderDetails");
            for (int i = 0; i < serviceProviderJsonArray.length(); i++) {
                serviceProviderObject = serviceProviderJsonArray.getJSONObject(i);
                serviceProviderIdArray.add(serviceProviderObject.getLong("serviceProviderId"));
                serviceProviderNameArray.add(serviceProviderObject.getString("serviceProviderName"));
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
}
