package com.goavega.mporium.Activities;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.TypedArray;
import android.graphics.Color;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.widget.DrawerLayout;
import android.text.Html;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.NumberPicker;
import android.widget.TextView;
import android.widget.Toast;
import com.goavega.mporium.ApiDatas.ApiHelper;
import com.goavega.mporium.ApiDatas.ApiUrls;
import com.goavega.mporium.Model.DashBoardModel;
import com.goavega.mporium.Classes.SaveUserLogin;
import com.goavega.mporium.R;
import com.goavega.mporium.Services.DashBoardService;
import com.goavega.mporium.adapters.DashBoardAdapter;
import com.goavega.mporium.db.AddDeviceTable;
import java.util.ArrayList;
import java.util.Date;

public class DashboardDisplayActivity extends BaseActivity implements NumberPicker.OnValueChangeListener {
    boolean f = true;
    AddDeviceTable addDeviceTable;
    Date sql;
    DashBoardModel dashBoardModel;
    ListView allNames;
    DashBoardService dashBoardService;
    ImageView dateFilterImage,
            menuIconImage,
            reviewService;
    private String[] navMenuTitles;
    private TypedArray navMenuIcons;
    private DrawerLayout mDrawerLayout;
    String userId;
    TextView custom_message;
    Toast toast;
    View layout;
    ApiHelper apiHelper;
    ApiUrls apiUrl;
    int datePickerMonthValue, datePickerYearValue;
    private ArrayList<DashBoardModel> dashBoardList = new ArrayList<DashBoardModel>();
    ArrayList<String> list;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dashboard_display);

        menuIconImage = (ImageView) findViewById(R.id.menuIcon);
        navMenuTitles = getResources().getStringArray(R.array.nav_drawer_items);
        navMenuIcons = getResources()
                .obtainTypedArray(R.array.nav_drawer_icons);
        set(navMenuTitles, navMenuIcons);
        mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        menuIconImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mDrawerLayout.openDrawer(Gravity.LEFT);
            }
        });

        LayoutInflater li = getLayoutInflater();
        layout = li.inflate(R.layout.success_message_display,
                (ViewGroup) findViewById(R.id.message_display));
        toast = new Toast(getApplicationContext());
        toast.setDuration(Toast.LENGTH_SHORT);
        toast.setGravity(Gravity.FILL_HORIZONTAL | Gravity.TOP, 0, 0);
        toast.setView(layout);
        custom_message=(TextView)layout.findViewById(R.id.messageText);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent gotoAddDevice = new Intent(getApplicationContext(), AddDeviceActivity.class);
                startActivity(gotoAddDevice);
            }
        });

        dashBoardModel = new DashBoardModel();
        dateFilterImage = (ImageView) findViewById(R.id.dateFilterImage);
        allNames = (ListView) findViewById(R.id.listNames);
        apiHelper = new ApiHelper(getApplicationContext());
        apiUrl = new ApiUrls();
        list = new ArrayList<String>();
        dashBoardService = new DashBoardService(DashboardDisplayActivity.this);
        userId = SaveUserLogin.getUserId(DashboardDisplayActivity.this).toString();
        addDeviceTable = new AddDeviceTable(DashboardDisplayActivity.this);
        DashBoardModel dashBoardModel = new DashBoardModel();
        dashBoardList = dashBoardService.setDeviceDetails();
        final DashBoardAdapter adapter = new DashBoardAdapter(DashboardDisplayActivity.this, dashBoardList);
        allNames.setAdapter(adapter);

        allNames.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, final int position, long id) {
                ImageView editDevice = (ImageView) view.findViewById(R.id.editDevice);
                ImageView deleteDevice = (ImageView) view.findViewById(R.id.deleteDevice);
                reviewService = (ImageView) view.findViewById(R.id.reviewService);

                deleteDevice.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        AlertDialog.Builder deleteDeviceDialog = new AlertDialog.Builder(DashboardDisplayActivity.this);
                        deleteDeviceDialog.setTitle(Html.fromHtml("<font color='#000'>Do you want to delete this device</font>"));
                        deleteDeviceDialog.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                boolean isRemoved;
                                AddDeviceTable deleteDevice;
                               // deleteDevice = new AddDeviceTable(DashboardDisplayActivity.this);
                                Long deviceId = new Long(dashBoardList.get(position).getDeviceId());
                             //   deleteDevice.remove(deviceId);
                                dashBoardList.remove(position);
                                adapter.notifyDataSetChanged();
                                adapter.notifyDataSetInvalidated();
                                isRemoved=dashBoardService.sendDeviceId(deviceId);
                                if(isRemoved) {
                                    layout.setBackgroundResource(R.color.lightRed);
                                    custom_message.setText(R.string.device_deleted);
                                    toast.show();
                                    // Toast.makeText(getApplicationContext(), R.string.device_deleted, Toast.LENGTH_LONG).show();
                                }
                            }
                        }).setNegativeButton("No", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                f = false;

                            }
                        });
                        AlertDialog alertDialog = deleteDeviceDialog.create();
                        alertDialog.show();
                        Button negativeButton = alertDialog.getButton(DialogInterface.BUTTON_NEGATIVE);
                        negativeButton.setTextColor(Color.RED);
                        Button positiveButton = alertDialog.getButton(DialogInterface.BUTTON_POSITIVE);
                        positiveButton.setTextColor(Color.RED);
                        if (!f) {
                            f = true;
                            alertDialog.dismiss();
                        }

                    }
                });


                editDevice.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent gotoSchedule = new Intent(DashboardDisplayActivity.this, EditDeviceActivity.class);
                        gotoSchedule.putExtra("deviceId", new Long(dashBoardList.get(position).getDeviceId()));
                        startActivity(gotoSchedule);
                    }
                });

                reviewService.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        //reviewService.setVisibility(View.INVISIBLE);
                        Intent gotoSchedule = new Intent(DashboardDisplayActivity.this, FeedBackActivity.class);
                        gotoSchedule.putExtra("deviceId", new Long(dashBoardList.get(position).getDeviceId()));
                        gotoSchedule.putExtra("deviceName", dashBoardList.get(position).getDeviceName());
                        startActivity(gotoSchedule);

                    }
                });
            }


        });

        dateFilterImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                show();

            }
        });


    }

    public void show() {
        final Dialog d = new Dialog(DashboardDisplayActivity.this);
        d.requestWindowFeature(Window.FEATURE_NO_TITLE);
        d.setContentView(R.layout.date_filter);
        final String[] values = {"January", "February", "March", "April", "May", "June", "July",
                "August", "September", "October", "November", "December"};
        TextView cancelText = (TextView) d.findViewById(R.id.cancelText);
        TextView doneText = (TextView) d.findViewById(R.id.doneText);
        final NumberPicker monthPicker = (NumberPicker) d.findViewById(R.id.monthPicker);
        final NumberPicker yearPicker = (NumberPicker) d.findViewById(R.id.yearPicker);
        monthPicker.setMaxValue(12);
        monthPicker.setMinValue(1);
        monthPicker.setDisplayedValues(values);
        yearPicker.setMinValue(2010);
        yearPicker.setMaxValue(2030);
        monthPicker.setWrapSelectorWheel(true);
        monthPicker.setOnValueChangedListener(this);
        yearPicker.setOnValueChangedListener(this);

        doneText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int position = monthPicker.getValue();
                datePickerMonthValue = position;
                datePickerYearValue = yearPicker.getValue();
                dashBoardList.clear();
                dashBoardList = dashBoardService.sendDateFilterData(datePickerMonthValue, datePickerYearValue);
                if(dashBoardList.isEmpty()) {
                    layout.setBackgroundResource(R.color.lightRed);
                    custom_message.setText(R.string.device_not_found);
                    toast.show();
                   //Toast.makeText(getApplicationContext(), "No Devices Found", Toast.LENGTH_LONG).show();
                }final DashBoardAdapter adapter = new DashBoardAdapter(DashboardDisplayActivity.this, dashBoardList);
                allNames.setAdapter(adapter);
                d.dismiss();
            }

        });

        cancelText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                d.dismiss();
            }
        });
        d.show();
    }

    @Override
    public void onValueChange(NumberPicker picker, int oldVal, int newVal) {

    }
}