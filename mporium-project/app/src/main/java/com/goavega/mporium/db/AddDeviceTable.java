package com.goavega.mporium.db;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import com.goavega.mporium.Model.DeviceDetailModel;
import com.goavega.mporium.R;

/**
 * Created by RAKESH on 12/1/2015.
 */
public class AddDeviceTable {
    MyDbOpenHelper openHelper;
    SQLiteDatabase db;
    DeviceDetailModel deviceDetailModel;
    public AddDeviceTable(Context context) {
        String dbName=context.getString(R.string.database_name);
        int dbVersion=context.getResources().getInteger(R.integer.database_version);
        openHelper=new MyDbOpenHelper(context,dbName,null,dbVersion);
    }
    public boolean insertDevice(DeviceDetailModel deviceDetailModel)
    {
        db=openHelper.getWritableDatabase();
        ContentValues cv=new ContentValues();
        cv.put(MyDbOpenHelper.DEVICE_NAME,deviceDetailModel.getDeviceName());
        cv.put(MyDbOpenHelper.deviceId,deviceDetailModel.getDeviceId());
        cv.put(MyDbOpenHelper.deviceUserId,deviceDetailModel.getUserId());
        cv.put(MyDbOpenHelper.PURCHASE_DATE,deviceDetailModel.getPurchaseDate());
        cv.put(MyDbOpenHelper.MODEL_NUMBER,deviceDetailModel.getModelNumber());
        cv.put(MyDbOpenHelper.SERIAL_NUMBER,deviceDetailModel.getSerialNumber());
        cv.put(MyDbOpenHelper.WARRANTY_PERIOD,deviceDetailModel.getWarrantyPeriod());
        cv.put(MyDbOpenHelper.MANUFACTURER,deviceDetailModel.getManufacturerName());
        cv.put(MyDbOpenHelper.SERVICEPROVIDER,deviceDetailModel.getServiceProviderName());
        cv.put(MyDbOpenHelper.SERVICE_SCHEDULE,deviceDetailModel.getServiceSchedulePeriod());
        cv.put(MyDbOpenHelper.SERVICE_SCHEDULE_DATE,deviceDetailModel.getServiceScheduleDate());
        cv.put(MyDbOpenHelper.SERVICE_REMAINING_DAYS,deviceDetailModel.getDays());
        cv.put(MyDbOpenHelper.WARRANTY_SCHEDULE_DATE,deviceDetailModel.getWarrantyScheduleDates());
        cv.put(MyDbOpenHelper.WARRANTY_REMAINING_YEARS,deviceDetailModel.getWarrantyYears());
        cv.put(MyDbOpenHelper.WARRANTY_REMAINING_DAYS,deviceDetailModel.getWarranty_remaining_days());
        long id=db.insert(MyDbOpenHelper.DEVICE_DETAILS,null,cv);
        db.close();
        if(id<0)
        {
            return false;
        }
        return true;
    }

    public Cursor readAllDevices()
    {
        db=openHelper.getReadableDatabase();
        Cursor c=db.query(MyDbOpenHelper.DEVICE_DETAILS, null, null, null, null, null, null);
        return c;
    }
    public void remove(long id){
       db=openHelper.getWritableDatabase();
       db.execSQL("DELETE FROM device_details WHERE deviceId = '" + id + "'");
    }
}

