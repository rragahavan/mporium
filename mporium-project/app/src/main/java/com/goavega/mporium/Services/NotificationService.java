package com.goavega.mporium.Services;

import android.content.Context;

import com.goavega.mporium.ApiDatas.ApiHelper;
import com.goavega.mporium.ApiDatas.ApiUrls;
import com.goavega.mporium.Classes.BaseModel;
import com.goavega.mporium.Classes.ConnectionDetector;
import com.goavega.mporium.Classes.SaveUserLogin;
import com.goavega.mporium.Model.NotificationModel;
import com.goavega.mporium.Model.RateModel;
import com.goavega.mporium.R;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by Administrator on 05-02-2016.
 */
public class NotificationService{
   private Context context;
   NotificationModel notificationModel;
   Boolean isSaved=false;
   SaveUserLogin saveUserLogin;
   ApiHelper apiHelper;
   ApiUrls apiUrl;

   ConnectionDetector connectionDetector;
   public NotificationService (Context context) {
      this.context=context;
   }
   public boolean sendNotificationDetails(NotificationModel notificationModel)
   {
      apiHelper=new ApiHelper(context);
      apiUrl=new ApiUrls();
      connectionDetector=new ConnectionDetector(context);
      if(connectionDetector.isNetworkAvailable()) {
         JSONObject notification;
         try {
            notification = new JSONObject();
            notification.put("notifyMeBefore", notificationModel.getRemainingDays());
            notification.put("notification", notificationModel.getNotification());
            notification.put("userId", notificationModel.getUserId());
            notification.put("registerId","1054546809182");
            notification.put("token","cUZtdiXBkwg:APA91bHOuQbfXApKhr7SJIm6xoF1vute7lLB_W8uWdzTz0tV4sAxf5vuOlZsTQbi1RlJ2sWmnaiQjtmo543lCTq2C3z0pAF5h6ovVA3MVtn1TnX0dKNA9aiTevEO8c7b1y2QbqzrX9wO");
            //notification.put("serviceProviderId", rateModel.getServiceProviderId());
            String notificationDetail = notification.toString();
            notificationDetail = apiHelper.sendUserDetail(notificationDetail, apiUrl.sendTokenUrl);
            notification = new JSONObject(notificationDetail);
            if (notification.getString("success").equals("true")) {
               isSaved = true;
            }
            else
            {
               notificationModel.setErrors(R.string.network_faliure);
            }

         } catch (JSONException e) {
            e.printStackTrace();
         }
      }
      return isSaved;
   }


   public NotificationModel setNotficationDetails()
   {
      notificationModel=new NotificationModel();
      apiHelper=new ApiHelper(context);
      apiUrl=new ApiUrls();
      String Id,notificstionDetail;
      JSONObject notifiaction;
      try {
         notifiaction=new JSONObject();
         notifiaction.put("userId", SaveUserLogin.getUserId(context));
         Id=notifiaction.toString();
         notificstionDetail=apiHelper.getUserDetails(Id, apiUrl.getNotificationUrl);
         notifiaction=new JSONObject(notificstionDetail);
         /*notificationModel.setRemainingDays(notifiaction.getInt("notifyMeBefore"));
         notificationModel.setNotification(notifiaction.getBoolean("notification"));
*/
         /*if(!(notifiaction.getInt("notifyMeBefore")==0))
            notificationModel.setRemainingDays(notifiaction.getInt("notifyMeBefore"));
         if(!(notifiaction.getBoolean("notifyMeBefore")))
            notificationModel.setNotification(notifiaction.getBoolean("notification"));
         */


            /*if(!(user.getString("mobileNumber").equals("null")))
            userDetailModel.setMobileNumber(user.getString("mobileNumber"));
*/
      }
      catch (JSONException e) {
         e.printStackTrace();
      }
      return notificationModel;
   }
}
