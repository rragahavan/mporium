package com.goavega.mporium.ServiceProvider.Activities;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.StrictMode;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
import com.goavega.mporium.Model.UserDetailModel;
import com.goavega.mporium.R;
import com.goavega.mporium.ServiceProvider.Services.CustomerService;

public class AddCustomerActivity extends AppCompatActivity {
    EditText nameText,
            mobileNumberText,
            addressLine1Text,
            addressLine2Text;
    AutoCompleteTextView cityText;
    Boolean isSaved;
    CustomerService customerService;
    UserDetailModel userDetailModel;
    Button addCustomerButton,
            searchButton;
    TextView custom_message;
    View layout;
    Toast toast;
    ImageView backArrow;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_customer);
        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);
        userDetailModel = new UserDetailModel();
        customerService = new CustomerService(AddCustomerActivity.this);
        LayoutInflater li = getLayoutInflater();
        layout = li.inflate(R.layout.success_message_display,
                (ViewGroup) findViewById(R.id.message_display));
        toast = new Toast(getApplicationContext());
        toast.setDuration(Toast.LENGTH_SHORT);
        toast.setGravity(Gravity.FILL_HORIZONTAL | Gravity.TOP, 0, 0);
        toast.setView(layout);
        backArrow=(ImageView)findViewById(R.id.backArrowImage);
        addCustomerButton = (Button) findViewById(R.id.signUpButton);
        nameText=(EditText)findViewById(R.id.nameText);
        addressLine1Text=(EditText)findViewById(R.id.addressLine1Text);
        addressLine2Text=(EditText)findViewById(R.id.addressLine2Text);
        cityText=(AutoCompleteTextView)findViewById(R.id.cityText);
        mobileNumberText=(EditText)findViewById(R.id.mobileNumberText);
        searchButton=(Button)findViewById(R.id.searchButton);


        searchButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                layout.setBackgroundResource(R.color.lightGreen);
                custom_message=(TextView)layout.findViewById(R.id.messageText);

                if(mobileNumberText.length()==10) {
                    userDetailModel.setPhoneNumber(mobileNumberText.getText().toString());
                    userDetailModel = customerService.sendPhoneUserData(userDetailModel);
                    if(userDetailModel.getErrors().size()!=0)
                    {
                        custom_message.setText(userDetailModel.getErrors().get(0));
                        toast.show();
                    }
                    else {
                        nameText.setText(userDetailModel.getName());
                        addressLine1Text.setText(userDetailModel.getAddressLine1());
                        addressLine2Text.setText(userDetailModel.getAddressLine2());
                        cityText.setText(userDetailModel.getCity());
                        mobileNumberText.setText(userDetailModel.getPhoneNumber());
                    }
                }
                else
                {

                    custom_message.setText(R.string.invalid_phone_number_length);
                    toast.show();

                }

           }
        });
        addCustomerButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                InputMethodManager keyboard = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
                keyboard.hideSoftInputFromWindow(v.getWindowToken(), 0);
                userDetailModel.setName(nameText.getText().toString());
                userDetailModel.setCity(cityText.getText().toString());
                userDetailModel.setPhoneNumber(mobileNumberText.getText().toString());
                userDetailModel.setAddressLine1(addressLine1Text.getText().toString());
                userDetailModel.setAddressLine2(addressLine2Text.getText().toString());
                isSaved = customerService.sendUserData(userDetailModel);
                if(isSaved)
                {
                    layout.setBackgroundResource(R.color.lightGreen);
                    custom_message=(TextView)layout.findViewById(R.id.messageText);
                    custom_message.setText(R.string.registration_success);
                    toast.show();
                    Intent gotoDashBoard = new Intent(AddCustomerActivity.this, ServiceProviderDashboardDisplayActivity.class);
                    startActivity(gotoDashBoard);
                }
                else {
                    if(userDetailModel.getErrors().size()!=0) {
                        layout.setBackgroundResource(R.color.lightGreen);
                        custom_message = (TextView) layout.findViewById(R.id.messageText);
                        custom_message.setText(userDetailModel.getErrors().get(0));
                        toast.show();
                    }
                }
            }
        });
        backArrow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent gotoDashboard = new Intent(AddCustomerActivity.this, ServiceProviderDashBoardActivity.class);
                startActivity(gotoDashboard);
            }
        });
    }
}
