package com.goavega.mporium.Activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;

import com.goavega.mporium.OCR.RecognizeTextActivity;
import com.goavega.mporium.R;
import com.goavega.mporium.ServiceProvider.Activities.ServiceProviderLoginActivity;

public class HomeActivity extends AppCompatActivity {
    Button customerButton,serviceProviderButton;

    Button ocrButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        /*Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
*/        customerButton=(Button)findViewById(R.id.customerButton);
        serviceProviderButton=(Button)findViewById(R.id.serviceProviderButton);

        ocrButton=(Button)findViewById(R.id.ocrButton);
        ocrButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i=new Intent(getApplicationContext(), RecognizeTextActivity.class);
                startActivity(i);
            }
        });

        customerButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent gotoCustomerLogin=new Intent(getApplicationContext(),LoginActivity.class);
                startActivity(gotoCustomerLogin);
            }
        });

        serviceProviderButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent gotoServiceProviderLogin=new Intent(getApplicationContext(), ServiceProviderLoginActivity.class);
                startActivity(gotoServiceProviderLogin);
            }
        });
    }
}
