package com.goavega.mporium.ServiceProvider.Activities;

import android.content.Intent;
import android.content.res.TypedArray;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Gravity;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.goavega.mporium.Activities.BaseActivity;
import com.goavega.mporium.R;
import com.goavega.mporium.ServiceProvider.Services.ServiceProviderDashboardService;

public class ServiceProviderDashBoardActivity extends NavigationDrawerActivity {

    private String[] navMenuTitles;
    private TypedArray navMenuIcons;
    private DrawerLayout mDrawerLayout;
    ImageView menuIconImage;
    ServiceProviderDashboardService dashboardService;
    Boolean isPresent;
    Button addCustomerButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_service_provider_dash_board);
        /*Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);*/
        menuIconImage = (ImageView) findViewById(R.id.menuIcon);
        navMenuTitles = getResources().getStringArray(R.array.service_provider_nav_drawer_item);
        navMenuIcons = getResources().obtainTypedArray(R.array.service_provider_nav_icons);
        set(navMenuTitles, navMenuIcons);
        mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        menuIconImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mDrawerLayout.openDrawer(Gravity.LEFT);
            }
        });
        addCustomerButton=(Button)findViewById(R.id.addCustomerButton);
        addCustomerButton.setOnClickListener(new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            Intent gotoAddCustomer = new Intent(ServiceProviderDashBoardActivity.this, AddCustomerActivity.class);
            startActivity(gotoAddCustomer);

        }}
        );

        dashboardService = new ServiceProviderDashboardService(ServiceProviderDashBoardActivity.this);
        isPresent=dashboardService.checkServiceProviderPresent();
        if(isPresent) {
            Intent gotoDashBoardDisplay = new Intent(ServiceProviderDashBoardActivity.this, ServiceProviderDashboardDisplayActivity.class);
            startActivity(gotoDashBoardDisplay);
            finish();
        }

    }

}
