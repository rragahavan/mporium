package com.goavega.mporium.Services;

import android.content.Context;
import com.goavega.mporium.ApiDatas.ApiHelper;
import com.goavega.mporium.ApiDatas.ApiUrls;
import com.goavega.mporium.Classes.ConnectionDetector;
import com.goavega.mporium.Model.ManufacturerDetailModel;
import com.goavega.mporium.R;
import com.goavega.mporium.db.ManufacturerTable;
import org.json.JSONException;
import org.json.JSONObject;
/**
 * Created by RAKESH on 1/10/2016.
 */
public class ManufacturerService {
    private Context context;
    ApiHelper apiHelper;
    ManufacturerTable dbOperations;
    ApiUrls apiUrl;
    ManufacturerDetailModel ManufacturerDetailModel;
    boolean isSaved=false,isInserted;
    Long manufacturerId;
    ConnectionDetector connectionDetector;

    public ManufacturerService(Context context){
        this.context = context;
    }

    public boolean sendManufacturerData(ManufacturerDetailModel manufacturerDetailModel)
    {
        apiHelper=new ApiHelper(context);
        dbOperations = new ManufacturerTable(context);
        apiUrl=new ApiUrls();
        connectionDetector=new ConnectionDetector(context);
        validate(manufacturerDetailModel);
        String ManufacturerDetail;
        String message;
        if(!(manufacturerDetailModel.getHasErrors())) {
            JSONObject Manufacturer = new JSONObject();
            try {
                Manufacturer.put("manufacturerName", manufacturerDetailModel.getName());
                Manufacturer.put("manufacturerCity", manufacturerDetailModel.getCity());
                //Manufacturer.put("manufacturerPhoneNumber", manufacturerDetailModel.getPhoneNumber());
                ManufacturerDetail = Manufacturer.toString();
                if(connectionDetector.isNetworkAvailable()) {
                    message = apiHelper.sendUserDetail(ManufacturerDetail, context.getString(R.string.url) + apiUrl.saveManufacturerUrl);
                    Manufacturer = new JSONObject(message);
                    if (Manufacturer.getString("success").equals("true")) {
                        manufacturerDetailModel.setManufacturerId(Manufacturer.getLong("manufacturerId"));
                        isInserted = dbOperations.insertManufacturer(manufacturerDetailModel);
                        if (isInserted) {
                            isSaved = true;
                        }
                    } else {
                        manufacturerDetailModel.setErrors(R.string.failure);
                    }
                }
                    else
                    {
                        manufacturerDetailModel.setErrors(R.string.network_faliure);
                    }

            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        return isSaved;
    }


    public void validate(ManufacturerDetailModel manufacturerDetailModel) {
        manufacturerDetailModel.Errors.clear();
        if (manufacturerDetailModel.getName().equals(""))
            manufacturerDetailModel.setErrors(R.string.manufacturerNameBlank);

        /*if(!(manufacturerDetailModel.getPhoneNumber().equals(""))) {
            if (manufacturerDetailModel.getPhoneNumber().length() < 10) {
                manufacturerDetailModel.setErrors(R.string.invalid_phone_number_length);
            }
        }*/
    }
}
