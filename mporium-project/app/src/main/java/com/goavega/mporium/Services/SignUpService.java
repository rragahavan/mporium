package com.goavega.mporium.Services;

import android.content.Context;
import com.goavega.mporium.ApiDatas.ApiHelper;
import com.goavega.mporium.ApiDatas.ApiUrls;
import com.goavega.mporium.Classes.ConnectionDetector;
import com.goavega.mporium.Classes.SaveUserLogin;
import com.goavega.mporium.Model.UserDetailModel;
import com.goavega.mporium.R;
import com.goavega.mporium.db.UserProfileTable;
import org.json.JSONException;
import org.json.JSONObject;
/**
 * Created by RAKESH on 1/6/2016.
 */
public class SignUpService{
    private Context context;
    ApiHelper apiHelper;
    UserProfileTable dbOperations;
    ApiUrls apiUrl;
    boolean isSaved=false,isInserted=false;
    ConnectionDetector connectionDetector;

    public SignUpService(Context context){
        this.context = context;
    }

    public boolean sendUserData(UserDetailModel userDetailModel)
    {
        apiHelper=new ApiHelper(context);
        dbOperations = new UserProfileTable(context);
        apiUrl=new ApiUrls();
        connectionDetector=new ConnectionDetector(context);
        validate(userDetailModel);
        String userDetail;
        String message,uid;
        Long userId;

        if(!(userDetailModel.getHasErrors())) {

            JSONObject User = new JSONObject();
            try {
                User.put("name", userDetailModel.getName());
                User.put("emailId", userDetailModel.getEmailAddress());
                User.put("city", userDetailModel.getCity());
                User.put("password", userDetailModel.getPassword());

                //User.put("serviceProviderPresent",false);
                userDetail = User.toString();
                if(connectionDetector.isNetworkAvailable()) {

                    //today
                    //if(userDetailModel.getUserType().equals("serviceProvider"))



                    message = apiHelper.sendUserDetail(userDetail, context.getString(R.string.url)+apiUrl.signUpUrl);
                    User = new JSONObject(message);
                    if (User.getString("success").equals("true")) {
                        isInserted = dbOperations.insertUser(userDetailModel);
                        if (isInserted) {
                            SaveUserLogin.setUserName(context,userDetailModel.getEmailAddress());
                            SaveUserLogin.setUserId(context, User.getLong("userId"));
                            isSaved = true;
                        }
                    } else {
                       userDetailModel.setErrors(R.string.emailExists);
                       userDetailModel.setErrors(R.string.failure);
                    }
                }
                else {
                    userDetailModel.setErrors(R.string.network_faliure);
                   }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        return isSaved;
    }

    public void validate(UserDetailModel userDetailModel) {
        userDetailModel.Errors.clear();
        if (userDetailModel.getName().equals(""))
            userDetailModel.setErrors(R.string.Name_Blank);
        if (userDetailModel.getEmailAddress().equals(""))
            userDetailModel.setErrors(R.string.emailId_Blank);
        if (!(userDetailModel.getEmailAddress().equals(""))) {
            boolean success=android.util.Patterns.EMAIL_ADDRESS.matcher(userDetailModel.getEmailAddress()).matches();
            if(!success) {
                userDetailModel.setErrors(R.string.invalid_email);
            }
            /*else
            {
                if(dbOperations.checkEmailExists(userDetailModel))
                    userDetailModel.setErrors(R.string.emailExists);
            }*/
        }
        if (userDetailModel.getCity().equals(""))
            userDetailModel.setErrors(R.string.cityBlank);
        if (userDetailModel.getPassword().equals(""))
            userDetailModel.setErrors(R.string.password_Blank);
        if (userDetailModel.getConfirmPassword().equals(""))
            userDetailModel.setErrors(R.string.confirmPassword_Blank);
        if(userDetailModel.getPassword().length()<8)
            userDetailModel.setErrors(R.string.invalid_password_length);
        if(!((userDetailModel.getPassword()).equals(userDetailModel.getConfirmPassword())))
            userDetailModel.setErrors(R.string.passwordMismatch);
        }
}
