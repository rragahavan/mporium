package com.goavega.mporium.ServiceProvider.Activities;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.StrictMode;
import android.support.v7.app.AppCompatActivity;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.goavega.mporium.Activities.DashBoardActivity;
import com.goavega.mporium.Activities.LoginActivity;
import com.goavega.mporium.Classes.SaveUserLogin;
import com.goavega.mporium.Model.ServiceProviderModel;
import com.goavega.mporium.Model.UserDetailModel;
import com.goavega.mporium.R;
import com.goavega.mporium.ServiceProvider.Model.ServiceProvidersModel;
import com.goavega.mporium.ServiceProvider.Services.ServiceProvidersSignUpService;
import com.goavega.mporium.Services.SignUpService;

public class ServiceProviderSignUpActivity extends AppCompatActivity {
    EditText firstNameText,
            emailAddressText,
            phoneNumberText,
            passwordText,
            confirmPasswordText;
    Button signUpButton;
    AutoCompleteTextView cityText;
    Boolean isSaved=false;
    SignUpService signUpService;
    ImageView backArrow;
    //UserDetailModel userDetailModel;

    ServiceProvidersModel serviceProvidersModel;
    ServiceProvidersSignUpService serviceProvidersSignUpService;

    TextView custom_message;
    Toast toast;
    View layout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_service_provider_signup);
        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);

        /*userDetailModel = new UserDetailModel();
        signUpService = new SignUpService(ServiceProviderSignUpActivity.this);
*/
        serviceProvidersModel=new ServiceProvidersModel();
        serviceProvidersSignUpService=new ServiceProvidersSignUpService(ServiceProviderSignUpActivity.this);

        backArrow = (ImageView) findViewById(R.id.backArrowImage);
        signUpButton = (Button) findViewById(R.id.signUpButton);
        firstNameText = (EditText) findViewById(R.id.firstNameText);
        emailAddressText = (EditText) findViewById(R.id.emailIdText);
        phoneNumberText=(EditText)findViewById(R.id.phoneNumberText);
        cityText = (AutoCompleteTextView) findViewById(R.id.cityText);
        passwordText = (EditText) findViewById(R.id.passwordText);
        confirmPasswordText = (EditText) findViewById(R.id.confirmPasswordText);
        LayoutInflater li = getLayoutInflater();
        layout = li.inflate(R.layout.success_message_display,
                (ViewGroup) findViewById(R.id.message_display));
        toast = new Toast(getApplicationContext());
        toast.setDuration(Toast.LENGTH_SHORT);
        toast.setGravity(Gravity.FILL_HORIZONTAL | Gravity.TOP, 0, 0);
        toast.setView(layout);

        ArrayAdapter userCityAdapter=new ArrayAdapter(this,android.R.layout.simple_list_item_1,getResources().getStringArray(R.array.cities));
        cityText.setAdapter(userCityAdapter);

        backArrow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent gotoLogin = new Intent(getApplicationContext(), ServiceProviderLoginActivity.class);
                startActivity(gotoLogin);
                overridePendingTransition(R.anim.left_to_right, R.anim.right_to_left);
            }
        });

        signUpButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                InputMethodManager keyboard = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
                keyboard.hideSoftInputFromWindow(v.getWindowToken(), 0);
                serviceProvidersModel.setName(firstNameText.getText().toString().toUpperCase());
                serviceProvidersModel.setEmailAddress(emailAddressText.getText().toString().trim());
                serviceProvidersModel.setCity(cityText.getText().toString());
                serviceProvidersModel.setPassword(passwordText.getText().toString());

                serviceProvidersModel.setPhoneNumber(phoneNumberText.getText().toString());

                serviceProvidersModel.setConfirmPassword(confirmPasswordText.getText().toString());
                serviceProvidersModel.setServiceProviderLogin(true);
                isSaved = serviceProvidersSignUpService.sendUserData(serviceProvidersModel);
                if(isSaved)
                {
                    SaveUserLogin.setServiceProviderEmail(getApplicationContext(), serviceProvidersModel.getEmailAddress());
                    SaveUserLogin.setServiceProviderName(getApplicationContext(),  serviceProvidersModel.getName());
                    layout.setBackgroundResource(R.color.lightGreen);
                    custom_message=(TextView)layout.findViewById(R.id.messageText);
                    custom_message.setText(R.string.registration_success);
                    toast.show();

                    //  Toast.makeText(SignUpActivity.this, R.string.registration_success, Toast.LENGTH_LONG).show();
                    Intent gotoDashBoard = new Intent(getApplicationContext(), ServiceProviderDashBoardActivity.class);
                    startActivity(gotoDashBoard);
                }
                else {
                    layout.setBackgroundResource(R.color.lightGreen);
                    custom_message=(TextView)layout.findViewById(R.id.messageText);
                    custom_message.setText( serviceProvidersModel.getErrors().get(0));
                    toast.show();
                    /*for(int i=0;i<userDetailModel.getErrors().size();i++)
                    {
                        Toast.makeText(SignUpActivity.this,userDetailModel.getErrors().get(i), Toast.LENGTH_LONG).show();
                    }*/
                }
            }

        });
    }
}


