package com.goavega.mporium.ServiceProvider.Activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.InputType;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.goavega.mporium.Activities.DashBoardActivity;
import com.goavega.mporium.Classes.SaveUserLogin;
import com.goavega.mporium.Model.UserDetailModel;
import com.goavega.mporium.R;
import com.goavega.mporium.ServiceProvider.Model.ServiceProvidersModel;
import com.goavega.mporium.ServiceProvider.Services.ServiceProviderUpdateService;
import com.goavega.mporium.Services.UpdateProfileService;
import com.goavega.mporium.db.UserProfileTable;

public class ServiceProviderUpdateProfile extends AppCompatActivity {
    EditText nameText,
            mobileNumberText,
            emailAddressText,
            addressLine1Text,
            addressLine2Text,
            cityText;
    Boolean isUpdated;
    Button saveButton;
    TextView cancelText;
    String mobileNumber,
            addressLine1,
            addressLine2;
    UserProfileTable dbOperations;
    //UserDetailModel userDetailModel;
    //UpdateProfileService updateProfileService;
    ServiceProvidersModel serviceProvidersModel;
    ServiceProviderUpdateService serviceProviderUpdateService;
    ImageView editEmail,
            editName,
            editLocation,
            backArrow;
    TextView custom_message;
    Toast toast;
    View layout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_service_provider_update_profile);
        /*Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
*/
        saveButton=(Button)findViewById(R.id.saveButton);
        Long id= SaveUserLogin.getPrefServiceProviderId(ServiceProviderUpdateProfile.this);
        //Toast.makeText(getApplicationContext(), "id"+id,Toast.LENGTH_LONG).show();

        //updateProfileService=new ServiceP(ServiceProviderUpdateProfile.this);
        serviceProviderUpdateService=new ServiceProviderUpdateService(ServiceProviderUpdateProfile.this);


        LayoutInflater li = getLayoutInflater();
        layout = li.inflate(R.layout.success_message_display,
                (ViewGroup) findViewById(R.id.message_display));
        toast = new Toast(getApplicationContext());
        toast.setDuration(Toast.LENGTH_SHORT);
        toast.setGravity(Gravity.FILL_HORIZONTAL | Gravity.TOP, 0, 0);
        toast.setView(layout);
       // dbOperations=new UserProfileTable(UpdateProfileActivity.this);
        backArrow=(ImageView)findViewById(R.id.backArrowImage);
        cancelText=(TextView)findViewById(R.id.cancelText);
        nameText=(EditText)findViewById(R.id.nameText);
        emailAddressText=(EditText)findViewById(R.id.emailAddressText);
        addressLine1Text=(EditText)findViewById(R.id.addressLine1Text);
        addressLine2Text=(EditText)findViewById(R.id.addressLine2Text);
        cityText=(EditText)findViewById(R.id.cityText);
        mobileNumberText=(EditText)findViewById(R.id.mobileNumberText);
        editEmail=(ImageView)findViewById(R.id.editEmail);
        editLocation=(ImageView)findViewById(R.id.editLocation);
        editName=(ImageView)findViewById(R.id.editName);
        serviceProvidersModel=new ServiceProvidersModel();
        serviceProvidersModel=serviceProviderUpdateService.setUserDetails();
        emailAddressText.setText(serviceProvidersModel.getEmailAddress());
        addressLine1=serviceProvidersModel.getAddressLine1();
        addressLine2=serviceProvidersModel.getAddressLine2();
        mobileNumber=serviceProvidersModel.getPhoneNumber();
        nameText.setText(serviceProvidersModel.getName());
        addressLine1Text.setText(serviceProvidersModel.getAddressLine1());
        addressLine2Text.setText(serviceProvidersModel.getAddressLine2());
        cityText.setText(serviceProvidersModel.getCity());
        mobileNumberText.setText(serviceProvidersModel.getMobileNumber());
        cityText.setEnabled(false);
        cityText.setInputType(InputType.TYPE_NULL);
        emailAddressText.setEnabled(false);
        emailAddressText.setInputType(InputType.TYPE_NULL);
        nameText.setEnabled(false);
        nameText.setInputType(InputType.TYPE_NULL);

        backArrow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        editEmail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                emailAddressText.setEnabled(true);
                emailAddressText.setInputType(InputType.TYPE_CLASS_TEXT);
                emailAddressText.setFocusable(true);
                emailAddressText.requestFocus();

            }
        });
        editName.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                nameText.setEnabled(true);
                nameText.setInputType(InputType.TYPE_CLASS_TEXT);
                nameText.setFocusable(true);
                nameText.requestFocus();
            }
        });
        editLocation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                cityText.setEnabled(true);
                cityText.setInputType(InputType.TYPE_CLASS_TEXT);
                cityText.setFocusable(true);
                cityText.requestFocus();
            }
        });
        cancelText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        saveButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                serviceProvidersModel.setName(nameText.getText().toString());
                serviceProvidersModel.setEmailAddress(emailAddressText.getText().toString());
                serviceProvidersModel.setAddressLine1(addressLine1Text.getText().toString());
                serviceProvidersModel.setAddressLine2(addressLine2Text.getText().toString());
                serviceProvidersModel.setCity(cityText.getText().toString());
                serviceProvidersModel.setMobileNumber(mobileNumberText.getText().toString());
                isUpdated = serviceProviderUpdateService.sendUpdatedUserData(serviceProvidersModel);
                if (isUpdated) {
                    SaveUserLogin.setServiceProviderName(ServiceProviderUpdateProfile.this, nameText.getText().toString());
                    SaveUserLogin.setServiceProviderEmail(ServiceProviderUpdateProfile.this, emailAddressText.getText().toString());
                    layout.setBackgroundResource(R.color.lightGreen);
                    custom_message=(TextView)layout.findViewById(R.id.messageText);
                    custom_message.setText( R.string.update_success);
                    toast.show();
                   /*Toast.makeText(UpdateProfileActivity.this, R.string.update_success,
                            Toast.LENGTH_SHORT).show();*/
                    // finish();
                    Intent gotoUpdateProfile = new Intent(ServiceProviderUpdateProfile.this, ServiceProviderDashBoardActivity.class);
                    startActivity(gotoUpdateProfile);
                } else {
                    if(serviceProvidersModel.getErrors().size()!=0) {
                        layout.setBackgroundResource(R.color.lightGreen);
                        custom_message = (TextView) layout.findViewById(R.id.messageText);
                        custom_message.setText(serviceProvidersModel.getErrors().get(0));
                        toast.show();

                    }/*for (int i = 0; i < userDetailModel.getErrors().size(); i++) {
                        Toast.makeText(UpdateProfileActivity.this, userDetailModel.getErrors().get(i), Toast.LENGTH_LONG).show();
                    }*/

                }
            }
        });
    }

}