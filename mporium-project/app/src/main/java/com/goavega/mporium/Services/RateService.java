package com.goavega.mporium.Services;
import android.content.Context;

import com.goavega.mporium.ApiDatas.ApiHelper;
import com.goavega.mporium.ApiDatas.ApiUrls;
import com.goavega.mporium.Classes.ConnectionDetector;
import com.goavega.mporium.Model.RateModel;
import com.goavega.mporium.Classes.SaveUserLogin;
import com.goavega.mporium.R;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by Administrator on 08-02-2016.
 */
public class RateService {
    private Context context;
    Boolean isSaved=false;
    SaveUserLogin saveUserLogin;
    ApiHelper apiHelper;
    ApiUrls apiUrl;

    ConnectionDetector connectionDetector;
    public RateService(Context context) {
        this.context=context;
    }
    public boolean sendRatingDetails(RateModel rateModel)
    {
        apiHelper=new ApiHelper(context);
        apiUrl=new ApiUrls();

        connectionDetector=new ConnectionDetector(context);
        if(connectionDetector.isNetworkAvailable()) {
            JSONObject rating;
            try {
                rating = new JSONObject();
                rating.put("comments", rateModel.getUserComment());
                rating.put("ratings", rateModel.getServiceProviderRate());
                rating.put("user", rateModel.getUserId());
                rating.put("device", rateModel.getDeviceId());
                rating.put("serviceProvider", rateModel.getServiceProviderId());
                rating.put("manufacturerName", rateModel.getManufacturerName());
                rating.put("serviceProviderName", rateModel.getServiceProviderName());
                rating.put("ratingDate",rateModel.getServiceDate());
                String ratingDetail = rating.toString();
                ratingDetail = apiHelper.sendUserDetail(ratingDetail, context.getString(R.string.url)+apiUrl.ratingUrl);
                rating = new JSONObject(ratingDetail);
                if (rating.getString("success").equals("true")) {
                    isSaved = true;
                }
                else
                {
                    rateModel.setErrors(R.string.network_faliure);
                }

            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        return isSaved;
    }



    public RateModel getRatingDetails(RateModel rateModel) {
        JSONObject device;
        apiHelper=new ApiHelper(context);
        String deviceDetail;

        try {
            device = new JSONObject();
            device.put("device", rateModel.getDeviceId());
            device.put("user",SaveUserLogin.getUserId(context));
            String deviceId = device.toString();
            deviceDetail = apiHelper.getUserDetails(deviceId,context.getString(R.string.url) + apiUrl.getRatingUrl);
            device = new JSONObject(deviceDetail);
            if (device.getString("success").equals("true")) {
                device=new JSONObject(deviceDetail);
                rateModel.setDeviceName(device.getString("deviceName"));
                rateModel.setManufacturerName(device.getString("manufacturerName"));
                rateModel.setServiceProviderName(device.getString("serviceProviderName"));
                rateModel.setServiceProviderId(device.getLong("serviceProviderId"));
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return rateModel;
    }

}
