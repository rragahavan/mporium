package com.goavega.mporium.ServiceProvider.Activities;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Rect;
import android.os.Bundle;
import android.os.Handler;
import android.os.StrictMode;
import android.support.v7.app.AppCompatActivity;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.goavega.mporium.Classes.SaveUserLogin;
import com.goavega.mporium.R;
import com.goavega.mporium.ServiceProvider.Model.ServiceProvidersModel;
import com.goavega.mporium.ServiceProvider.Services.ServiceProvidersLoginService;
import com.goavega.mporium.Services.LoginService;

public class ServiceProviderLoginActivity extends AppCompatActivity {
    TextView signUpLinkText;
    Button loginButton;
    EditText passwordText,
            emailAddressText;
    LoginService loginService;
    ServiceProvidersLoginService serviceProvidersLoginService;
    boolean isValid=false;
    ServiceProvidersModel serviceProvidersModel;
    TextView forgotPasswordText;
    boolean isPresent=false;
    String emailAddress;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        if(SaveUserLogin.getPrefServiceProviderId(ServiceProviderLoginActivity.this)!=0)
        {
            Intent gotoServiceProviderDashBoard = new Intent(ServiceProviderLoginActivity.this,ServiceProviderDashBoardActivity.class);
            startActivity(gotoServiceProviderDashBoard);
            finish();
        }

        StrictMode.ThreadPolicy policy=new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);
        //loginService=new LoginService(ServiceProviderLoginActivity.this);
        serviceProvidersLoginService=new ServiceProvidersLoginService(ServiceProviderLoginActivity.this);
        serviceProvidersModel=new ServiceProvidersModel();
        passwordText=(EditText)findViewById(R.id.passwordText);
        emailAddressText=(EditText)findViewById(R.id.emailAddressText);
        forgotPasswordText=(TextView)findViewById(R.id.forgotText);
        signUpLinkText=(TextView)findViewById(R.id.signUpLinkText);
        loginButton=(Button)findViewById(R.id.loginButton);

        forgotPasswordText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                LayoutInflater li = getLayoutInflater();
                View layout = li.inflate(R.layout.customtoast,
                        (ViewGroup) findViewById(R.id.custom_toast_layout));
                Toast toast = new Toast(getApplicationContext());
                layout.setBackgroundResource(R.color.lightRed);
                toast.setDuration(Toast.LENGTH_SHORT);
                toast.setGravity(Gravity.FILL_HORIZONTAL | Gravity.TOP, 0, 0);
                toast.setView(layout);
                emailAddress=emailAddressText.getText().toString();
                if(emailAddress.equals(""))
                {
                    toast.show();
                }
                else
                {
                    final ProgressDialog dialog = ProgressDialog.show(ServiceProviderLoginActivity.this, "",
                            "Please wait...", true);
                    dialog.show();
                    final Handler handler = new Handler();
                    handler.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            dialog.dismiss();
                        }
                    }, 8000);
                    TextView custom_message=(TextView)layout.findViewById(R.id.forgotMessage);
                    isPresent=serviceProvidersLoginService.forgotPassword(emailAddress);
                    if(isPresent)
                    {
                        layout.setBackgroundResource(R.color.lightGreen);
                        custom_message.setText(R.string.reset_password);
                        toast.show();
                    }
                    else {
                        dialog.dismiss();
                        custom_message.setText(R.string.accountNotExist);
                        toast.show();
                    }
                }
            }
        });

        signUpLinkText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent gotoSignUp = new Intent(ServiceProviderLoginActivity.this,ServiceProviderSignUpActivity.class);
                startActivity(gotoSignUp);
                overridePendingTransition(R.anim.left_to_right,R.anim.right_to_left);
            }
        });

        RelativeLayout touchInterceptor = (RelativeLayout)findViewById(R.id.relativeMain);
        touchInterceptor.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (event.getAction() == MotionEvent.ACTION_DOWN) {
                    if (emailAddressText.isFocused() || passwordText.isFocusable()) {
                        Rect outRect = new Rect();
                        emailAddressText.getGlobalVisibleRect(outRect);
                        passwordText.getGlobalVisibleRect(outRect);
                        if (!outRect.contains((int) event.getRawX(), (int) event.getRawY())) {
                            emailAddressText.clearFocus();
                            passwordText.clearFocus();
                            InputMethodManager imm = (InputMethodManager) v.getContext().getSystemService(Context.INPUT_METHOD_SERVICE);
                            imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
                        }
                    }
                }
                return false;
            }
        });

        loginButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                InputMethodManager keyboard = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                keyboard.hideSoftInputFromWindow(v.getWindowToken(), 0);

                LayoutInflater li = getLayoutInflater();
                View layout = li.inflate(R.layout.success_message_display,
                        (ViewGroup) findViewById(R.id.message_display));
                Toast toast = new Toast(getApplicationContext());
                toast.setDuration(Toast.LENGTH_SHORT);
                toast.setGravity(Gravity.FILL_HORIZONTAL | Gravity.TOP, 0, 0);
                toast.setView(layout);
                serviceProvidersModel.setEmailAddress(emailAddressText.getText().toString().trim());
                serviceProvidersModel.setPassword(passwordText.getText().toString());
                serviceProvidersModel.setServiceProviderLogin(true);
                isValid = serviceProvidersLoginService.sendUserData(serviceProvidersModel);
                if (isValid) {
                    SaveUserLogin.setServiceProviderEmail(getApplicationContext(), serviceProvidersModel.getEmailAddress());

                    Intent gotoDashBoard = new Intent(ServiceProviderLoginActivity.this, ServiceProviderDashBoardActivity.class);
                    startActivity(gotoDashBoard);

                } else {
                    if(serviceProvidersModel.getErrors().size()!=0) {
                        layout.setBackgroundResource(R.color.lightRed);
                        TextView custom_message = (TextView) layout.findViewById(R.id.messageText);
                        custom_message.setText(serviceProvidersModel.getErrors().get(0));
                        toast.show();
                        //Toast.makeText(LoginActivity.this, userDetailModel.getErrors().get(i), Toast.LENGTH_LONG).show();
                    }
                }
            }

        });
    }
}