package com.goavega.mporium.Model;

import com.goavega.mporium.Classes.BaseModel;

/**
 * Created by Administrator on 05-02-2016.
 */
public class NotificationModel extends BaseModel {
    Long userId;
    Boolean notification;
    Integer remainingDays;
    String token;

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public Boolean getNotification() {
        return notification;
    }

    public void setNotification(Boolean notification) {
        this.notification = notification;
    }

    public Integer getRemainingDays() {
        return remainingDays;
    }

    public void setRemainingDays(Integer remainingDays) {
        this.remainingDays = remainingDays;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }


}
