package com.goavega.mporium.db;

import android.content.ContentValues;
import android.content.Context;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.goavega.mporium.Model.UserDetailModel;
import com.goavega.mporium.R;

/**
 * Created by RAKESH on 11/18/2015.
 */
public class UserProfileTable {
    MyDbOpenHelper openHelper;
    private Context context;
    SQLiteDatabase db;
    UserDetailModel userDetailModel;
    SharedPreferences loginPreference;
    UserProfileTable dbOperations;
    boolean isEmailExists=false;
    public UserProfileTable(Context context) {
        String dbName=context.getString(R.string.database_name);
        int dbVersion=context.getResources().getInteger(R.integer.database_version);
        this.context=context;
        openHelper=new MyDbOpenHelper(context,dbName,null,dbVersion);
    }

    public boolean insertUser(UserDetailModel userDetailModel)
    {
        Cursor c = readAllUsers();
        if (c != null && c.getCount() > 0) {
            while (c.moveToNext()) {
                if (((userDetailModel.getEmailAddress()).equals(c.getString(c.getColumnIndex(MyDbOpenHelper.emailAddress))))) {
                    isEmailExists=true;
                    break;
                }
            }
        }



        db=openHelper.getWritableDatabase();
        ContentValues cv=new ContentValues();
        cv.put(MyDbOpenHelper.firstName,userDetailModel.getName());
        cv.put(MyDbOpenHelper.lastName,userDetailModel.getLastName());
        cv.put(MyDbOpenHelper.emailAddress,userDetailModel.getEmailAddress());
        cv.put(MyDbOpenHelper.addressLine1,userDetailModel.getAddressLine1());
        cv.put(MyDbOpenHelper.addressLine2,userDetailModel.getAddressLine2());
        cv.put(MyDbOpenHelper.city,userDetailModel.getCity());
        cv.put(MyDbOpenHelper.phoneNumber,userDetailModel.getMobileNumber());
        cv.put(MyDbOpenHelper.password,userDetailModel.getPassword());
        long id=db.insert(MyDbOpenHelper.userDetails,null,cv);
        db.close();
        if(id<0)
        {
            return false;
        }
        return true;
    }

    public Cursor readAllUsers()
    {
        db=openHelper.getReadableDatabase();
        Cursor c=db.query(MyDbOpenHelper.userDetails, null, null, null, null, null, null);
        return c;

    }

    public UserDetailModel getUserDetails()
    {
        loginPreference=context.getSharedPreferences("LoginData", Context.MODE_PRIVATE);
        String userEmail=loginPreference.getString("emailKey","N/A");
        userDetailModel=new UserDetailModel();
        dbOperations=new UserProfileTable(context);
        Cursor c = dbOperations.readAllUsers();
        if (c != null && c.getCount() > 0) {
            while (c.moveToNext()) {
                if ((userEmail.equals(c.getString(c.getColumnIndex(MyDbOpenHelper.emailAddress))))) {
                    userDetailModel.setEmailAddress(userEmail);
                    userDetailModel.setName(c.getString(c.getColumnIndex(MyDbOpenHelper.firstName)));
                    userDetailModel.setLastName(c.getString(c.getColumnIndex(MyDbOpenHelper.lastName)));
                    userDetailModel.setAddressLine1(c.getString(c.getColumnIndex(MyDbOpenHelper.addressLine1)));
                    userDetailModel.setAddressLine2(c.getString(c.getColumnIndex(MyDbOpenHelper.addressLine2)));
                    userDetailModel.setPhoneNumber(c.getString(c.getColumnIndex(MyDbOpenHelper.phoneNumber)));
                    userDetailModel.setCity(c.getString(c.getColumnIndex(MyDbOpenHelper.city)));
                }
            }
        }
        return userDetailModel;
    }


    public boolean checkEmailExists(UserDetailModel userDetailModel)
    {
        Cursor c = readAllUsers();
        if (c != null && c.getCount() > 0) {
            while (c.moveToNext()) {
                if (((userDetailModel.getEmailAddress()).equals(c.getString(c.getColumnIndex(MyDbOpenHelper.emailAddress))))) {
                    isEmailExists=true;
                    break;
                }
            }
        }
        if(isEmailExists)
           return true;
        else
            return  false;
    }

    public int updateUserProfile(UserDetailModel userDetailModel)
    {
        db = openHelper.getWritableDatabase();
        ContentValues cv = new ContentValues();
        cv.put(MyDbOpenHelper.firstName,userDetailModel.getName());
        cv.put(MyDbOpenHelper.lastName,userDetailModel.getLastName());
        cv.put(MyDbOpenHelper.addressLine1,userDetailModel.getAddressLine1());
        cv.put(MyDbOpenHelper.addressLine2,userDetailModel.getAddressLine2());
        cv.put(MyDbOpenHelper.city,userDetailModel.getCity());
        cv.put(MyDbOpenHelper.phoneNumber,userDetailModel.getMobileNumber());
        int count = db.update(MyDbOpenHelper.userDetails, cv, "emailAddress = ?", new String[]{userDetailModel.getEmailAddress()});
        db.close();
        return count;
    }

}