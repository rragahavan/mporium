package com.goavega.mporium.ServiceProvider.Services;

import android.content.Context;

import com.goavega.mporium.ApiDatas.ApiHelper;
import com.goavega.mporium.ApiDatas.ApiUrls;
import com.goavega.mporium.Classes.ConnectionDetector;
import com.goavega.mporium.Classes.SaveUserLogin;
import com.goavega.mporium.Model.UserDetailModel;
import com.goavega.mporium.R;
import com.goavega.mporium.ServiceProvider.Model.ServiceProvidersModel;
import com.goavega.mporium.db.UserProfileTable;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by Administrator on 04-04-2016.
 */
public class ServiceProvidersLoginService {
    UserProfileTable dbOperations;
    private Context context;
    SaveUserLogin saveUserLogin;
    ApiHelper apiHelper;
    ApiUrls apiUrl;
    String userLoginDetail;
    boolean isValid=false;
    ConnectionDetector connectionDetector;

    public ServiceProvidersLoginService(Context context){
        this.context = context;
    }

    public boolean sendUserData(ServiceProvidersModel serviceProvidersModel)
    {
        apiHelper=new ApiHelper(context);
        saveUserLogin=new SaveUserLogin();
        dbOperations = new UserProfileTable(context);
        apiUrl=new ApiUrls();
        connectionDetector=new ConnectionDetector(context);
        validate(serviceProvidersModel);
        String message;
        if(!(serviceProvidersModel.getHasErrors()))
        {
            JSONObject User = new JSONObject();
            try {
                User.put("emailId", serviceProvidersModel.getEmailAddress());
                User.put("password", serviceProvidersModel.getPassword());
              //  User.put("userType",true);
                //User.put("serviceProviderPresent",serviceProvidersModel.isServiceProviderLogin());
                userLoginDetail = User.toString();
                if(connectionDetector.isNetworkAvailable()) {

                    message = apiHelper.sendUserDetail(userLoginDetail, context.getString(R.string.url)+apiUrl.serviceProviderLoginUrl);
                    User = new JSONObject(message);
                    if (User.getString("success").equals("true")) {
                        //if (dbOperations.checkEmailExists(serviceProvidersModel)) {
                        SaveUserLogin.setServiceProviderName(context,User.getString("serviceProviderName"));
                        SaveUserLogin.setPrefServiceProviderId(context, User.getLong("serviceProviderId"));
                        isValid = true;
                        //}
                    } else {
                        serviceProvidersModel.setErrors(R.string.invalid_password_email);
                    }
                }
                else
                {
                    serviceProvidersModel.setErrors(R.string.network_faliure);
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        return isValid;
    }

    public void validate(ServiceProvidersModel serviceProvidersModel) {
        serviceProvidersModel.Errors.clear();
        if (serviceProvidersModel.getEmailAddress().equals(""))
            serviceProvidersModel.setErrors(R.string.enter_email);
        if (serviceProvidersModel.getPassword().equals(""))
            serviceProvidersModel.setErrors(R.string.enter_password);
    }

    public boolean forgotPassword(String emailAddress)
    {
        JSONObject user;
        boolean isPresent=false;
        connectionDetector=new ConnectionDetector(context);
        apiHelper=new ApiHelper(context);
        apiUrl=new ApiUrls();
        String userEmailId, userDetail;
        try {

            user = new JSONObject();
            user.put("emailId", emailAddress);
            userEmailId = user.toString();
            if(connectionDetector.isNetworkAvailable()) {
                userDetail = apiHelper.sendUserDetail(userEmailId, context.getString(R.string.url) + apiUrl.serviceProviderForgotPasswordUrl);
                user = new JSONObject(userDetail);
                if (user.getString("success").equals("true")) {
                    isPresent = true;
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return isPresent;
    }
}
