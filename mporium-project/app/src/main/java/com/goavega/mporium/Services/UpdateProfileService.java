package com.goavega.mporium.Services;

import android.content.Context;

import com.goavega.mporium.ApiDatas.ApiHelper;
import com.goavega.mporium.ApiDatas.ApiUrls;
import com.goavega.mporium.Classes.ConnectionDetector;
import com.goavega.mporium.Classes.SaveUserLogin;
import com.goavega.mporium.Model.UserDetailModel;
import com.goavega.mporium.R;
import com.goavega.mporium.db.UserProfileTable;
import org.json.JSONException;
import org.json.JSONObject;
/**
 * Created by RAKESH on 1/8/2016.
 */
public class UpdateProfileService {
    private Context context;
    UserDetailModel userDetailModel;
    JSONObject user;
    ApiHelper apiHelper;
    UserProfileTable dbOperations;
    ApiUrls apiUrl;
    Boolean isUpdated=false;
    String userEmailId,
            userDetail;
    ConnectionDetector connectionDetector;

    public UpdateProfileService(Context context) {
    this.context=context;
    }

    public UserDetailModel setUserDetails()
    {
        userDetailModel=new UserDetailModel();
        apiHelper=new ApiHelper(context);
        connectionDetector=new ConnectionDetector(context);
        dbOperations = new UserProfileTable(context);
        apiUrl=new ApiUrls();
        String Id;
        try {
                user=new JSONObject();
                user.put("user", SaveUserLogin.getUserId(context));
                Id=user.toString();
            if(connectionDetector.isNetworkAvailable()) {
                userDetail = apiHelper.getUserDetails(Id, context.getString(R.string.url) + apiUrl.getUserUrl);
                user = new JSONObject(userDetail);
                userDetailModel.setEmailAddress(user.getString("emailId"));
                userDetailModel.setName(user.getString("userName"));
//                userDetailModel.setLastName(user.getString("lastName"));
                if (!(user.getString("addressLine1").equals("null")))
                    userDetailModel.setAddressLine1(user.getString("addressLine1"));
                if (!(user.getString("addressLine2").equals("null")))
                    userDetailModel.setAddressLine2(user.getString("addressLine2"));
                if (!(user.getString("city").equals("null")))
                    userDetailModel.setCity(user.getString("city"));
                if (!(user.getString("mobileNumber").equals("null")))
                    userDetailModel.setMobileNumber(user.getString("mobileNumber"));
            }
            else {
                userDetailModel.setErrors(R.string.network_faliure);
            }

        }
        catch (JSONException e) {
            e.printStackTrace();
        }
        return userDetailModel;
    }


    public boolean sendUpdatedUserData(UserDetailModel userDetailModel)
    {
        apiUrl=new ApiUrls();
        String message;
        dbOperations = new UserProfileTable(context);
        validate(userDetailModel);
        if(!(userDetailModel.getHasErrors())) {
            JSONObject User = new JSONObject();
            String userDetail = null;
            try {
                User.put("emailId", userDetailModel.getEmailAddress());
                User.put("name", userDetailModel.getName());
                //  User.put("lastName", userDetailModel.getLastName());
                User.put("addressLine1", userDetailModel.getAddressLine1());
                User.put("addressLine2", userDetailModel.getAddressLine2());
                User.put("city", userDetailModel.getCity());
                User.put("mobileNumber", userDetailModel.getMobileNumber());
                User.put("user", SaveUserLogin.getUserId(context));
                userDetail = User.toString();
                message=apiHelper.sendUpdateUserDetails(userDetail,context.getString(R.string.url)+ apiUrl.updateUrl);
                User=new JSONObject(message);
                if(User.getString("success").equals("true"))
                {
                    /*int updateCount = dbOperations.updateUserProfile(userDetailModel);
                    if(updateCount>0)
*/                  //  {
                        isUpdated=true;
                    //}
                }
                else
                {
                    userDetailModel.setErrors(R.string.update_failure);
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        return isUpdated;
    }
    public void validate(UserDetailModel userDetailModel) {
        userDetailModel.Errors.clear();
        if (userDetailModel.getName().equals(""))
            userDetailModel.setErrors(R.string.Name_Blank);
        if (userDetailModel.getCity().equals(""))
            userDetailModel.setErrors(R.string.cityBlank);
        if (!(userDetailModel.getMobileNumber().equals("")))
        if(userDetailModel.getMobileNumber().length()!=10)
            userDetailModel.setErrors(R.string.invalid_phone_number_length);

    }
}
