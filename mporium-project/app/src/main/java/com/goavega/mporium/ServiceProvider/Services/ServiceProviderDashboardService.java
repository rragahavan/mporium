package com.goavega.mporium.ServiceProvider.Services;

import android.content.Context;
import android.widget.Toast;
import com.goavega.mporium.ApiDatas.ApiHelper;
import com.goavega.mporium.ApiDatas.ApiUrls;
import com.goavega.mporium.Classes.ConnectionDetector;
import com.goavega.mporium.Classes.SaveUserLogin;
import com.goavega.mporium.R;
import com.goavega.mporium.ServiceProvider.Model.ServiceProviderDashboardModel;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by Administrator on 06-04-2016.
 */
public class ServiceProviderDashboardService {
    private Context context;
    ApiHelper apiHelper;
    ApiUrls apiUrl;
    ServiceProviderDashboardModel dashBoardModel;
    ConnectionDetector connectionDetector;

    public ServiceProviderDashboardService(Context context) {
        this.context = context;
    }
    private void showToast(String message){
        Toast.makeText(context, message, Toast.LENGTH_SHORT).show();
    }

    public ArrayList<ServiceProviderDashboardModel> setDeviceDetails(){
        apiHelper = new ApiHelper(context);
        apiUrl = new ApiUrls();
        //dashBoardModel=new DashBoardModel();
        connectionDetector = new ConnectionDetector(context);
        String message, deviceDetail;
        String serviceScheduleDate,warrantyPeriodDate;
        ArrayList<ServiceProviderDashboardModel> dashBoardList = new ArrayList<ServiceProviderDashboardModel>();
        JSONObject device = new JSONObject();
        JSONArray deviceJsonArray=new JSONArray();
        try {
            device.put("serviceProvider", SaveUserLogin.getPrefServiceProviderId(context));
            //device.put("serviceProvider", 4L);
            deviceDetail = device.toString();
            if (connectionDetector.isNetworkAvailable()) {
                message = apiHelper.getUserDetails(deviceDetail, context.getString(R.string.url) + apiUrl.serviceProviderDashboardDisplayUrl);
                device = new JSONObject(message);
                deviceJsonArray = device.getJSONArray("customers");
                if (device.getString("success").equals("true")) {
                    for (int i = 0; i < deviceJsonArray.length(); i++) {
                        device = deviceJsonArray.getJSONObject(i);
                        dashBoardModel=new ServiceProviderDashboardModel();
                        dashBoardModel.setDeviceName(device.getString("deviceName"));
                     //   dashBoardModel.setDeviceId(device.getLong("deviceId"));
                        dashBoardModel.setCustomerCity(device.getString("customerCity"));
                        dashBoardModel.setCustomerName(device.getString("customerName"));
                        if(!device.getString("mobileNumber").equals("null"))
                        dashBoardModel.setCustomerMobileNumber(device.getString("mobileNumber"));
                        //checke here
                        dashBoardModel.setServiceProvideId(SaveUserLogin.getPrefServiceProviderId(context));
                        //dashBoardModel.setPurchaseDate((device.getString("purchaseDate")).substring(0, 10));
                        //dashBoardModel.setWarrantyPeriods(device.getInt("warrantyPeriod"));
                        //dashBoardModel.setManufacturerName(device.getString("manufacturerName"));
                        dashBoardModel.setServiceSchedulePeriod(device.getInt("serviceSchedule"));
                        dashBoardModel.setServiceScheduleDate((device.getString("serviceScheduleDate")).substring(0, 10));
                        //dashBoardModel.setWarrantyPeriodExpire((device.getString("warrantyDate")).substring(0, 10));
                        //dashBoardModel.setWarrantyRemainingDays(device.getInt("warrantyRemaining"));
                        dashBoardModel.setServiceRemainingDays(device.getInt("serviceRemaining"));
                        //dashBoardModel.setServiceScheduleDate((device.getString("serviceScheduleDate")).substring(0, 10));
                        dashBoardModel.setColor("#FF0000");
                        int serviceRemainingDaysInt = device.getInt("serviceRemaining");
                        // Toast.makeText(getApplicationContext(),"serviceperiod"+serviceRemainingDaysInt,Toast.LENGTH_LONG).show();
                        if (serviceRemainingDaysInt>0) {
                            // dashBoardModel.setServiceRemainingDays(device.getInt("serviceRemaining")). + "Days Left");
                            if (serviceRemainingDaysInt<10)
                            {
                                dashBoardModel.setColor("#FF0000");
                            }
                            if((serviceRemainingDaysInt>10)||(serviceRemainingDaysInt<20))
                            {
                                dashBoardModel.setColor("#F1A006");
                            }
                            if(serviceRemainingDaysInt>20)
                            {
                                dashBoardModel.setColor("#00a300");
                            }

                        }

                        if (serviceRemainingDaysInt<0){

                            dashBoardModel.setColor("#FF0000");

                        }
                        dashBoardList.add(dashBoardModel);

                    }

                } else {
                    dashBoardModel.setErrors(R.string.data_available);
                }
            } else {
                dashBoardModel.setErrors(R.string.network_faliure);

            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return dashBoardList;
    }


    public ArrayList<ServiceProviderDashboardModel> searchByCustomerName(String filterData,String filterOption){
        apiHelper = new ApiHelper(context);
        apiUrl = new ApiUrls();
        //dashBoardModel=new DashBoardModel();
        connectionDetector = new ConnectionDetector(context);
        String message, deviceDetail;
        String serviceScheduleDate,warrantyPeriodDate;
        ArrayList<ServiceProviderDashboardModel> dashBoardList = new ArrayList<ServiceProviderDashboardModel>();
        JSONObject device = new JSONObject();
        JSONArray deviceJsonArray=new JSONArray();
        try {
            if(filterOption.equals("Customer_Name"))
                device.put("name", filterData);
            if(filterOption.equals("Customer_City"))
                device.put("city",filterData);
            if(filterOption.equals("Device_Name"))
                device.put("deviceName",filterData);
            device.put("filterOption",filterOption);
            device.put("serviceProvider", SaveUserLogin.getPrefServiceProviderId(context));
            deviceDetail = device.toString();
            if (connectionDetector.isNetworkAvailable()) {
               // message = apiHelper.getUserDetails(deviceDetail, context.getString(R.string.url) + apiUrl.searchByCustomerNameUrl);
               // message = apiHelper.getUserDetails(deviceDetail, context.getString(R.string.url) + apiUrl.serviceProviderDashboardDisplayUrl);
                message = apiHelper.getUserDetails(deviceDetail, context.getString(R.string.url) + apiUrl.customerCityUrl);
                device = new JSONObject(message);
                deviceJsonArray = device.getJSONArray("customerInfo");
                if (device.getString("success").equals("true")) {
                    for (int i = 0; i < deviceJsonArray.length(); i++) {
                        device = deviceJsonArray.getJSONObject(i);
                       // if((device.getString("customerName")).equals(filterData)) {
                            dashBoardModel = new ServiceProviderDashboardModel();
                            dashBoardModel.setDeviceName(device.getString("deviceName"));
                            //   dashBoardModel.setDeviceId(device.getLong("deviceId"));
                            dashBoardModel.setCustomerCity(device.getString("customerCity"));
                            dashBoardModel.setCustomerName(device.getString("customerName"));
                            if(!device.getString("mobileNumber").equals("null"))
                            dashBoardModel.setCustomerMobileNumber(device.getString("mobileNumber"));
                            //checke here
                            dashBoardModel.setServiceProvideId(SaveUserLogin.getPrefServiceProviderId(context));
                            //dashBoardModel.setPurchaseDate((device.getString("purchaseDate")).substring(0, 10));
                            //dashBoardModel.setWarrantyPeriods(device.getInt("warrantyPeriod"));
                            //dashBoardModel.setManufacturerName(device.getString("manufacturerName"));
                            dashBoardModel.setServiceSchedulePeriod(device.getInt("serviceSchedule"));
                            dashBoardModel.setServiceScheduleDate((device.getString("serviceScheduleDate")).substring(0, 10));
                            //dashBoardModel.setWarrantyPeriodExpire((device.getString("warrantyDate")).substring(0, 10));
                            //dashBoardModel.setWarrantyRemainingDays(device.getInt("warrantyRemaining"));
                            dashBoardModel.setServiceRemainingDays(device.getInt("serviceRemaining"));
                            //dashBoardModel.setServiceScheduleDate((device.getString("serviceScheduleDate")).substring(0, 10));
                            dashBoardModel.setColor("#FF0000");
                            int serviceRemainingDaysInt = device.getInt("serviceRemaining");
                            // Toast.makeText(getApplicationContext(),"serviceperiod"+serviceRemainingDaysInt,Toast.LENGTH_LONG).show();
                            if (serviceRemainingDaysInt > 0) {
                                // dashBoardModel.setServiceRemainingDays(device.getInt("serviceRemaining")). + "Days Left");
                                if (serviceRemainingDaysInt < 10) {
                                    dashBoardModel.setColor("#FF0000");
                                }
                                if ((serviceRemainingDaysInt > 10) || (serviceRemainingDaysInt < 20)) {
                                    dashBoardModel.setColor("#F1A006");
                                }
                                if (serviceRemainingDaysInt > 20) {
                                    dashBoardModel.setColor("#00a300");
                                }

                            }

                            if (serviceRemainingDaysInt < 0) {

                                dashBoardModel.setColor("#FF0000");

                            }
                            dashBoardList.add(dashBoardModel);
                       // }
                    }

                } else {
                    dashBoardModel.setErrors(R.string.data_available);
                }
            } else {
                dashBoardModel.setErrors(R.string.network_faliure);

            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return dashBoardList;
    }


    public boolean checkServiceProviderPresent()
    {
        boolean isPresent=false;
        apiHelper = new ApiHelper(context);
        apiUrl = new ApiUrls();
        dashBoardModel=new ServiceProviderDashboardModel();
        connectionDetector = new ConnectionDetector(context);
        String message, deviceDetail;
        JSONObject device = new JSONObject();
        try {
            device.put("serviceProvider", SaveUserLogin.getPrefServiceProviderId(context));
            deviceDetail = device.toString();
            if (connectionDetector.isNetworkAvailable()) {
                message = apiHelper.getUserDetails(deviceDetail, context.getString(R.string.url) + apiUrl.getServiceProviderIdUrl);
                device = new JSONObject(message);
                if (device.getString("success").equals("true"))
                    isPresent=true;
            }
            else
            {
                dashBoardModel.setErrors(R.string.network_faliure);
                showToast("network failure");
                //isPresent=false;
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return isPresent;
    }
}

