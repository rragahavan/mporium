package com.goavega.mporium.db;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import com.goavega.mporium.Model.ServiceProviderModel;
import com.goavega.mporium.R;

/**
 * Created by RAKESH on 11/24/2015.
 */
public class ServiceProviderTable {
    SQLiteDatabase db;
    MyDbOpenHelper openHelper;

    public ServiceProviderTable(Context context) {
        String dbName=context.getString(R.string.database_name);
        int dbVersion=context.getResources().getInteger(R.integer.database_version);
        openHelper=new MyDbOpenHelper(context,dbName,null,dbVersion);
    }

    public boolean insertServiceProvider(ServiceProviderModel serviceProviderModel)
    {
        db=openHelper.getWritableDatabase();
        ContentValues cv=new ContentValues();
        cv.put(MyDbOpenHelper.serviceProviderName,serviceProviderModel.getName());
        cv.put(MyDbOpenHelper.serviceProviderUserId,serviceProviderModel.getUserId());
        cv.put(MyDbOpenHelper.serviceProviderCity,serviceProviderModel.getCity());
        cv.put(MyDbOpenHelper.serviceProviderPhoneNumber,serviceProviderModel.getPhoneNumber());
        long id=db.insert(MyDbOpenHelper.serviceProviderDetails,null,cv);

        db.close();
        if(id<0)
        {
            return false;
        }
        return true;
    }

    public Cursor readAllServiceProvider()
    {
        db=openHelper.getReadableDatabase();
        Cursor sp=db.query(openHelper.serviceProviderDetails,null,null,null,null,null,null);
        return sp;
    }
}



