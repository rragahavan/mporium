package com.goavega.mporium.Classes;

import android.content.Context;
import android.content.res.Configuration;

/**
 * Created by Administrator on 14-03-2016.
 */
public class DeviceDetector {
    private Context context;

    public DeviceDetector(Context context) {
        this.context = context;
    }

    public static boolean isTablet(Context context) {
        return (context.getResources().getConfiguration().screenLayout
                & Configuration.SCREENLAYOUT_SIZE_MASK)
                >= Configuration.SCREENLAYOUT_SIZE_LARGE;
    }
}
