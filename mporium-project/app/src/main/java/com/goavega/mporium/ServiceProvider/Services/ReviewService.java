package com.goavega.mporium.ServiceProvider.Services;

import android.content.Context;
import com.goavega.mporium.ApiDatas.ApiHelper;
import com.goavega.mporium.ApiDatas.ApiUrls;
import com.goavega.mporium.Classes.ConnectionDetector;
import com.goavega.mporium.Classes.SaveUserLogin;
import com.goavega.mporium.Model.RateModel;
import com.goavega.mporium.R;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import java.util.ArrayList;

/**
 * Created by Administrator on 13-04-2016.
 */
public class ReviewService {
    private Context context;
    ApiHelper apiHelper;
    RateModel rateModel;
    ApiUrls apiUrl;
    ConnectionDetector connectionDetector;
    public ReviewService(Context context) {
        this.context=context;
    }
    public ArrayList<RateModel> setRatingDetails() {
        //JSONObject device;
        apiHelper = new ApiHelper(context);
        apiUrl = new ApiUrls();
        connectionDetector = new ConnectionDetector(context);
        String message;
        ArrayList<RateModel> rateList = new ArrayList<RateModel>();
        JSONObject device = new JSONObject();
        JSONArray deviceJsonArray=new JSONArray();
        try {
            device = new JSONObject();
 //           device.put("device", rateModel.getDeviceId());
            device.put("serviceProvider", SaveUserLogin.getPrefServiceProviderId(context));
            String deviceId = device.toString();
            if (connectionDetector.isNetworkAvailable()) {
                message = apiHelper.getUserDetails(deviceId, context.getString(R.string.url) + apiUrl.reviewServiceUrl);
                device = new JSONObject(message);
                deviceJsonArray = device.getJSONArray("ratingsInfo");
                if (device.getString("success").equals("true")) {
                    for (int i = 0; i < deviceJsonArray.length(); i++) {
                        device = deviceJsonArray.getJSONObject(i);
                        rateModel=new RateModel();
                        rateModel.setServiceProviderRate(Float.parseFloat(device.getString("ratings")));
                        rateModel.setDeviceName(device.getString("deviceName"));
                        rateModel.setUserName(device.getString("customerName"));
                        rateModel.setMobileNumber(device.getString("userMobileNumber"));
                        rateModel.setUserComment(device.getString("comments"));
                        rateModel.setServiceProviderRates(device.getString("ratings"));
                        rateList.add(rateModel);
                    }
                }
            }
            else {
                rateModel.setErrors(R.string.network_faliure);
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }
        return rateList;
    }


    public ArrayList<RateModel> searchByCustomerName(String filterData,String filterOption) {

        apiHelper = new ApiHelper(context);
        apiUrl = new ApiUrls();
        connectionDetector = new ConnectionDetector(context);
        String message;
        ArrayList<RateModel> rateList = new ArrayList<RateModel>();
        JSONObject device;
        JSONArray deviceJsonArray=new JSONArray();
        try {
            device = new JSONObject();
            device.put("serviceProvider", SaveUserLogin.getPrefServiceProviderId(context));
            device.put("deviceName",filterData);
            String deviceId = device.toString();
            if (connectionDetector.isNetworkAvailable()) {
                //not changed the url
                message = apiHelper.getUserDetails(deviceId, context.getString(R.string.url) + apiUrl.searchRatingUrl);
                device = new JSONObject(message);
                deviceJsonArray = device.getJSONArray("ratingsInfo");
                if (device.getString("success").equals("true")) {
                    for (int i = 0; i < deviceJsonArray.length(); i++) {
                        device = deviceJsonArray.getJSONObject(i);
                        rateModel=new RateModel();
                        rateModel.setServiceProviderRate(Float.parseFloat(device.getString("ratings")));
                        rateModel.setDeviceName(device.getString("deviceName"));
                        rateModel.setUserName(device.getString("customerName"));
                        //if(!device.getString("userMobileNumber").equals("null"))
                        rateModel.setMobileNumber(device.getString("userMobileNumber"));
                        //if(!device.getString("comments").equals("null"))
                        rateModel.setUserComment(device.getString("comments"));
                        rateModel.setServiceProviderRates(device.getString("ratings"));
                        rateList.add(rateModel);
                    }
                }
            }
            else {
                rateModel.setErrors(R.string.network_faliure);
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }
        return rateList;
    }
}
