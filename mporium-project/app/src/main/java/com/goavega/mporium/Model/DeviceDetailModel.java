package com.goavega.mporium.Model;

import com.goavega.mporium.Classes.BaseModel;
import java.sql.Date;

/**
 * Created by RAKESH on 12/24/2015.
 */
public class DeviceDetailModel extends BaseModel {
    String deviceName;
    String purchaseDate;
    Long warranty_remaining_days;
    String servviceProviderPhoneNumber,
            serviceProviderCity;
    Date purchaseDateDb;
    String modelNumber;
    Integer frequencyService;
    String serialNumber;
    Integer warrantyPeriod;
    Integer serviceSchedulePeriod;
    String serviceScheduleDates,
            warrantyScheduleDates;
    long days,
         warrantyYears;
    Long userId;
    String manufacturerName;
    String serviceProviderName;
    String serviceScheduleDate;
    Long deviceId;
    Long serviceProviderId;
    Long manufacturerId;
    public Long getWarranty_remaining_days() {
        return warranty_remaining_days;
    }

    public void setWarranty_remaining_days(Long warranty_remaining_days) {
        this.warranty_remaining_days = warranty_remaining_days;
    }

    public String getServviceProviderPhoneNumber() {
        return servviceProviderPhoneNumber;
    }

    public void setServviceProviderPhoneNumber(String servviceProviderPhoneNumber) {
        this.servviceProviderPhoneNumber = servviceProviderPhoneNumber;
    }

    public String getServiceProviderCity() {
        return serviceProviderCity;
    }

    public void setServiceProviderCity(String serviceProviderCity) {
        this.serviceProviderCity = serviceProviderCity;
    }


    public Date getPurchaseDateDb() {
        return this.purchaseDateDb;
    }

    public void setPurchaseDateDb(Date purchaseDateDb) {
        this.purchaseDateDb = purchaseDateDb;
    }


    public Integer getFrequencyService() {
        return frequencyService;
    }

    public void setFrequencyService(Integer frequencyService) {
        this.frequencyService = frequencyService;
    }

    public Integer getServiceSchedulePeriod() {
        return serviceSchedulePeriod;
    }

    public void setServiceSchedulePeriod(Integer serviceSchedulePeriod) {
        this.serviceSchedulePeriod = serviceSchedulePeriod;
    }

    public Integer getWarrantyPeriod() {
        return warrantyPeriod;
    }

    public void setWarrantyPeriod(Integer warrantyPeriod) {
        this.warrantyPeriod = warrantyPeriod;
    }


    public Long getDeviceId() {
        return deviceId;
    }

    public void setDeviceId(Long deviceId) {
        this.deviceId = deviceId;
    }


    public Long getManufacturerId() {
        return manufacturerId;
    }

    public void setManufacturerId(Long manufacturerId) {
        this.manufacturerId = manufacturerId;
    }

    public Long getServiceProviderId() {
        return serviceProviderId;
    }

    public void setServiceProviderId(Long serviceProviderId) {
        this.serviceProviderId = serviceProviderId;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public String getServiceScheduleDates() {
        return serviceScheduleDates;
    }

    public void setServiceScheduleDates(String serviceScheduleDates) {
        this.serviceScheduleDates = serviceScheduleDates;
    }

    public String getWarrantyScheduleDates() {
        return warrantyScheduleDates;
    }

    public void setWarrantyScheduleDates(String warrantyScheduleDates) {
        this.warrantyScheduleDates = warrantyScheduleDates;
    }

    public long getDays() {
        return days;
    }

    public void setDays(long days) {
        this.days = days;
    }

    public long getWarrantyYears() {
        return warrantyYears;
    }

    public void setWarrantyYears(long warrantyYears) {
        this.warrantyYears = warrantyYears;
    }

    public String getManufacturerName() {
        return manufacturerName;
    }

    public void setManufacturerName(String manufacturerName) {
        this.manufacturerName = manufacturerName;
    }

    public String getServiceProviderName() {
        return serviceProviderName;
    }

    public void setServiceProviderName(String serviceProviderName) {
        this.serviceProviderName = serviceProviderName;
    }


  public String getDeviceName() {
  return deviceName;
  }

  public void setDeviceName(String deviceName) {
  this.deviceName = deviceName;
  }

  public String getPurchaseDate() {
   return purchaseDate;
  }

  public void setPurchaseDate(String purchaseDate) {
   this.purchaseDate = purchaseDate;
 }

 public String getModelNumber() {
  return modelNumber;
 }

 public void setModelNumber(String modelNumber) {
  this.modelNumber = modelNumber;
 }

 public String getSerialNumber() {
  return serialNumber;
 }

 public void setSerialNumber(String serialNumber) {
  this.serialNumber = serialNumber;
 }

  public String getServiceScheduleDate() {
  return serviceScheduleDate;
 }

 public void setServiceScheduleDate(String serviceScheduleDate) {
  this.serviceScheduleDate = serviceScheduleDate;
 }

}
