package com.goavega.mporium.Services;

import android.content.Context;
import com.goavega.mporium.ApiDatas.ApiHelper;
import com.goavega.mporium.ApiDatas.ApiUrls;
import com.goavega.mporium.Classes.ConnectionDetector;
import com.goavega.mporium.Classes.SaveUserLogin;
import com.goavega.mporium.Model.UserDetailModel;
import com.goavega.mporium.R;
import com.goavega.mporium.db.UserProfileTable;
import org.json.JSONException;
import org.json.JSONObject;
/**
 * Created by RAKESH on 1/6/2016.
 */
public class LoginService {
    UserProfileTable dbOperations;
    private Context context;
    SaveUserLogin saveUserLogin;
    ApiHelper apiHelper;
    ApiUrls apiUrl;
    String userLoginDetail;
    boolean isValid=false;
    ConnectionDetector connectionDetector;

    public LoginService(Context context){
        this.context = context;
    }

    public boolean sendUserData(UserDetailModel userDetailModel)
    {
        apiHelper=new ApiHelper(context);
        saveUserLogin=new SaveUserLogin();
        dbOperations = new UserProfileTable(context);
        apiUrl=new ApiUrls();
        connectionDetector=new ConnectionDetector(context);
        validate(userDetailModel);
        String message;
        if(!(userDetailModel.getHasErrors()))
        {
            JSONObject User = new JSONObject();
            try {
                User.put("emailId", userDetailModel.getEmailAddress());
                User.put("password", userDetailModel.getPassword());
                userLoginDetail = User.toString();
                if(connectionDetector.isNetworkAvailable()) {

                    message = apiHelper.sendUserDetail(userLoginDetail, context.getString(R.string.url)+apiUrl.loginUrl);
                    User = new JSONObject(message);
                    if (User.getString("success").equals("true")) {
                        //if (dbOperations.checkEmailExists(userDetailModel)) {
                            SaveUserLogin.setUserName(context,User.getString("name"));
                            SaveUserLogin.setUserId(context, User.getLong("userId"));
                            isValid = true;
                        //}
                    } else {
                        userDetailModel.setErrors(R.string.invalid_password_email);
                    }
                }
                else
                {
                    userDetailModel.setErrors(R.string.network_faliure);
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        return isValid;
    }

    public void validate(UserDetailModel userDetailModel) {
        userDetailModel.Errors.clear();
        if (userDetailModel.getEmailAddress().equals(""))
            userDetailModel.setErrors(R.string.enter_email);
        if (userDetailModel.getPassword().equals(""))
            userDetailModel.setErrors(R.string.enter_password);
    }

    public boolean forgotPassword(String emailAddress)
    {
        JSONObject user;
        boolean isPresent=false;
        connectionDetector=new ConnectionDetector(context);
        apiHelper=new ApiHelper(context);
        apiUrl=new ApiUrls();
        String userEmailId, userDetail;
        try {

            user = new JSONObject();
            user.put("emailId", emailAddress);
            userEmailId = user.toString();
            if(connectionDetector.isNetworkAvailable()) {
                userDetail = apiHelper.sendUserDetail(userEmailId, context.getString(R.string.url) + apiUrl.forgotPasswordUrl);
                user = new JSONObject(userDetail);
                if (user.getString("success").equals("true")) {
                    isPresent = true;
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return isPresent;
    }
}
