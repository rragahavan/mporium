package com.goavega.mporium.ServiceProvider.Activities;

import android.content.res.TypedArray;
import android.os.Bundle;
import android.support.v4.widget.DrawerLayout;
import android.view.Gravity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;
import com.goavega.mporium.ApiDatas.ApiHelper;
import com.goavega.mporium.ApiDatas.ApiUrls;
import com.goavega.mporium.Model.RateModel;
import com.goavega.mporium.R;
import com.goavega.mporium.ServiceProvider.Adapters.ReviewServiceAdapter;
import com.goavega.mporium.ServiceProvider.Services.ReviewService;
import com.goavega.mporium.Services.RateService;
import java.util.ArrayList;


public class ReviewServiceActivity extends NavigationDrawerActivity {
    private String[] navMenuTitles;
    private TypedArray navMenuIcons;
    private DrawerLayout mDrawerLayout;
    ImageView menuIconImage;
    private ArrayList<RateModel> ratingList = new ArrayList<RateModel>();
    Long deviceId;
    ApiHelper apiHelper;
    String customerName,
            deviceName,
             ratingValue;
    ApiUrls apiUrls;
    String filterOption;
    RateService ratingService;
    ReviewService reviewService;
    RateModel rateModel;
    TextView dashboardTitle;
    ImageView ratingFilter;
    ListView serviceProviderList;
    String date;
    EditText searchText;
    Spinner searchFilterSpinner;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_review_service);
        //
        menuIconImage = (ImageView) findViewById(R.id.menuIcon);
        navMenuTitles = getResources().getStringArray(R.array.service_provider_nav_drawer_item);
        navMenuIcons = getResources()
                .obtainTypedArray(R.array.service_provider_nav_icons);
        set(navMenuTitles, navMenuIcons);
        mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        menuIconImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mDrawerLayout.openDrawer(Gravity.LEFT);
            }
        });
        dashboardTitle=(TextView)findViewById(R.id.dashboardTitle);
        ratingFilter = (ImageView) findViewById(R.id.ratingFilterImage);
        searchFilterSpinner=(Spinner)findViewById(R.id.searchFilter);
        reviewService=new ReviewService(ReviewServiceActivity.this);
        searchText=(EditText)findViewById(R.id.searchText);
        rateModel = new RateModel();
        serviceProviderList = (ListView) findViewById(R.id.serviceProviderList);
        ratingList=reviewService.setRatingDetails();
        //ratingList.add(rateModel);
        final ReviewServiceAdapter adapter = new ReviewServiceAdapter(ReviewServiceActivity.this, ratingList);
        serviceProviderList.setAdapter(adapter);



        /*ArrayAdapter<String> searchFilterAdapter=new ArrayAdapter(this,R.layout.support_simple_spinner_dropdown_item,getResources().getStringArray(R.array.search_filter));
        searchFilterSpinner.setAdapter(searchFilterAdapter);
*/
        ratingFilter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                searchText.setVisibility(View.VISIBLE);
                searchText.requestFocus();
                dashboardTitle.setVisibility(View.GONE);
                //searchFilterSpinner.setVisibility(View.VISIBLE);
                //ratingValue=
                searchFilterSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

               @Override
               public void onItemSelected(AdapterView<?> arg0, View arg1, int arg2, long arg3) {
               String item = searchFilterSpinner.getSelectedItem().toString();
               if (item.equals("Customer_Name")) {
                   filterOption = "Customer_Name";
               } else if (item.equals("Customer_City")) {
                  filterOption = "Customer_City";
               } else {
                filterOption = "Device_Name";
              }
              }

               @Override
               public void onNothingSelected(AdapterView<?> parent) {
               }
               }
                );
                customerName=searchText.getText().toString();
               // if((!customerName.equals(""))&&(!filterOption.equals(""))) {
                    if(!customerName.equals("")){
                        filterOption="";
                    ratingList=reviewService.searchByCustomerName(customerName,filterOption);
                    final ReviewServiceAdapter adapter = new ReviewServiceAdapter(ReviewServiceActivity.this, ratingList);
                      serviceProviderList.setAdapter(adapter);
                }
            }
        });
    }

}


