package com.goavega.mporium.ServiceProvider.Services;

import android.content.Context;
import com.goavega.mporium.ApiDatas.ApiHelper;
import com.goavega.mporium.ApiDatas.ApiUrls;
import com.goavega.mporium.Classes.ConnectionDetector;
import com.goavega.mporium.Classes.SaveUserLogin;
import com.goavega.mporium.Model.UserDetailModel;
import com.goavega.mporium.R;
import com.goavega.mporium.ServiceProvider.Model.ServiceProvidersModel;
import com.goavega.mporium.db.UserProfileTable;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by Administrator on 05-04-2016.
 */
public class ServiceProviderUpdateService {
    private Context context;
    //UserDetailModel serviceProvidersModel;
    ServiceProvidersModel serviceProvidersModel;
    JSONObject user;
    ApiHelper apiHelper;
    UserProfileTable dbOperations;
    ApiUrls apiUrl;
    Boolean isUpdated=false;
    String userEmailId,
            userDetail;
    ConnectionDetector connectionDetector;

    public ServiceProviderUpdateService(Context context) {
        this.context=context;
    }

    public ServiceProvidersModel setUserDetails()
    {

          serviceProvidersModel=new ServiceProvidersModel();  
        //serviceProvidersModel=new UserDetailModel();
        apiHelper=new ApiHelper(context);
        connectionDetector=new ConnectionDetector(context);
        //dbOperations = new UserProfileTable(context);
        apiUrl=new ApiUrls();
        String Id;
        try {
            user=new JSONObject();
            user.put("serviceProviders", SaveUserLogin.getPrefServiceProviderId(context));
            Id=user.toString();
            if(connectionDetector.isNetworkAvailable()) {
                userDetail = apiHelper.getUserDetails(Id, context.getString(R.string.url) + apiUrl.serviceProviderDetailsUrl);
                user = new JSONObject(userDetail);
                serviceProvidersModel.setEmailAddress(user.getString("emailId"));
                serviceProvidersModel.setName(user.getString("serviceProviderName"));
//                serviceProvidersModel.setLastName(user.getString("lastName"));
                if (!(user.getString("addressLine1").equals("null")))
                    serviceProvidersModel.setAddressLine1(user.getString("addressLine1"));
                if (!(user.getString("addressLine2").equals("null")))
                    serviceProvidersModel.setAddressLine2(user.getString("addressLine2"));
                if (!(user.getString("city").equals("null")))
                    serviceProvidersModel.setCity(user.getString("city"));
                if (!(user.getString("mobileNumber").equals("null")))
                    serviceProvidersModel.setMobileNumber(user.getString("mobileNumber"));
            }
            else {
                serviceProvidersModel.setErrors(R.string.network_faliure);
            }

        }
        catch (JSONException e) {
            e.printStackTrace();
        }
        return serviceProvidersModel;
    }


    public boolean sendUpdatedUserData(ServiceProvidersModel serviceProvidersModel)
    {
        apiUrl=new ApiUrls();
        String message;
        dbOperations = new UserProfileTable(context);
        validate(serviceProvidersModel);
        if(!(serviceProvidersModel.getHasErrors())) {
            JSONObject User = new JSONObject();
            String userDetail = null;
            try {
                User.put("emailId", serviceProvidersModel.getEmailAddress());
                User.put("serviceProviderName", serviceProvidersModel.getName());
                //  User.put("lastName", serviceProvidersModel.getLastName());
                User.put("address1", serviceProvidersModel.getAddressLine1());
                User.put("address2", serviceProvidersModel.getAddressLine2());
                User.put("serviceProviderCity", serviceProvidersModel.getCity());
                User.put("serviceProviderPhoneNumber", serviceProvidersModel.getMobileNumber());
              //  User.put("user", SaveUserLogin.getUserId(context));
                User.put("serviceProviders", SaveUserLogin.getPrefServiceProviderId(context));
                userDetail = User.toString();
                message=apiHelper.sendUpdateUserDetails(userDetail,context.getString(R.string.url)+ apiUrl.serviceProviderUpdateUrl);
                User=new JSONObject(message);
                if(User.getString("success").equals("true"))
                {
                    /*int updateCount = dbOperations.updateUserProfile(serviceProvidersModel);
                    if(updateCount>0)
*/                  //  {
                    isUpdated=true;
                    //}
                }
                else
                {
                    serviceProvidersModel.setErrors(R.string.update_failure);
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        return isUpdated;
    }
    public void validate(ServiceProvidersModel serviceProvidersModel) {
        serviceProvidersModel.Errors.clear();
        if (serviceProvidersModel.getName().equals(""))
            serviceProvidersModel.setErrors(R.string.Name_Blank);
        if (serviceProvidersModel.getCity().equals(""))
            serviceProvidersModel.setErrors(R.string.cityBlank);
        if (!(serviceProvidersModel.getMobileNumber().equals("")))
            if(serviceProvidersModel.getMobileNumber().length()!=10)
                serviceProvidersModel.setErrors(R.string.invalid_phone_number_length);
    }
}



