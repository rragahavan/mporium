package com.goavega.mporium.Services;

import android.content.Context;
import com.goavega.mporium.ApiDatas.ApiHelper;
import com.goavega.mporium.ApiDatas.ApiUrls;
import com.goavega.mporium.Classes.SaveUserLogin;
import com.goavega.mporium.Model.ServiceProviderModel;
import com.goavega.mporium.R;
import com.goavega.mporium.db.ServiceProviderTable;
import org.json.JSONException;
import org.json.JSONObject;
/**
 * Created by RAKESH on 1/10/2016.
 */
public class ServiceProviderService {
    private Context context;
    ApiHelper apiHelper;
    ServiceProviderTable dbOperations;
    ApiUrls apiUrl;
    boolean isSaved=false,isInserted;

    public ServiceProviderService(Context context){
        this.context = context;
    }

     public boolean sendServiceProviderData(ServiceProviderModel serviceProviderModel)
    {
        apiHelper=new ApiHelper(context);
        dbOperations = new ServiceProviderTable(context);
        apiUrl=new ApiUrls();
        String ServiceProviderDetail;
        validate(serviceProviderModel);
        String message;

        if(!(serviceProviderModel.getHasErrors())) {
            JSONObject serviceProvider = new JSONObject();
            try {
                serviceProvider.put("serviceProviderName", serviceProviderModel.getName());
                serviceProvider.put("serviceProviderCity", serviceProviderModel.getCity());
                serviceProvider.put("serviceProviderPhoneNumber", serviceProviderModel.getPhoneNumber());
                serviceProvider.put("id",serviceProviderModel.getServiceProviderId());
                serviceProvider.put("user", SaveUserLogin.getUserId(context));
                ServiceProviderDetail = serviceProvider.toString();
                message=apiHelper.sendUserDetail(ServiceProviderDetail,context.getString(R.string.url)+apiUrl.saveServiceProviderUrl);
                serviceProvider=new JSONObject(message);
               if(serviceProvider.getString("success").equals("true"))
                {
                    serviceProviderModel.setServiceProviderId(serviceProvider.getLong("serviceProviderId"));
                    isInserted = dbOperations.insertServiceProvider(serviceProviderModel);
                    if(isInserted)
                    {
                        isSaved=true;
                    }
                }
                else
                {
                    serviceProviderModel.setErrors(R.string.failure);
                }

            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        return isSaved;
    }


    public boolean sendsServiceProviderData(ServiceProviderModel serviceProviderModel)
    {
        apiHelper=new ApiHelper(context);
        dbOperations = new ServiceProviderTable(context);
        apiUrl=new ApiUrls();
        String ServiceProviderDetail;
//        validate(serviceProviderModel);
        String message;

        if(!(serviceProviderModel.getHasErrors())) {
            JSONObject serviceProvider = new JSONObject();
            try {
                serviceProvider.put("serviceProviderName", serviceProviderModel.getName());
                serviceProvider.put("serviceProviderCity", serviceProviderModel.getCity());
                serviceProvider.put("serviceProviderPhoneNumber", serviceProviderModel.getPhoneNumber());
                serviceProvider.put("id",serviceProviderModel.getServiceProviderId());
                serviceProvider.put("user", SaveUserLogin.getUserId(context));
                ServiceProviderDetail = serviceProvider.toString();
                message=apiHelper.sendUpdateUserDetails(ServiceProviderDetail,context.getString(R.string.url)+ apiUrl.updateServiceProviderUrl);
                serviceProvider=new JSONObject(message);
                if(serviceProvider.getString("success").equals("true"))
                {
                    serviceProviderModel.setServiceProviderId(serviceProvider.getLong("serviceProviderId"));
                    isInserted = dbOperations.insertServiceProvider(serviceProviderModel);
                    if(isInserted)
                    {
                        isSaved=true;
                    }
                }
                else
                {
                    serviceProviderModel.setErrors(R.string.failure);
                }

            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        return isSaved;
    }


    public void validate(ServiceProviderModel serviceProviderModel) {
        serviceProviderModel.Errors.clear();
        if (serviceProviderModel.getName().equals(""))
            serviceProviderModel.setErrors(R.string.serviceProviderNameBlank);

        if(!(serviceProviderModel.getPhoneNumber().equals(""))) {
            if (serviceProviderModel.getPhoneNumber().length() < 10) {
                serviceProviderModel.setErrors(R.string.invalid_phone_number_length);
            }
        }
    }
}
