package com.goavega.mporium.Activities;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
import com.goavega.mporium.ApiDatas.ApiHelper;
import com.goavega.mporium.ApiDatas.ApiUrls;
import com.goavega.mporium.Model.DeviceDetailModel;
import com.goavega.mporium.Classes.SaveUserLogin;
import com.goavega.mporium.Model.ManufacturerDetailModel;
import com.goavega.mporium.Model.ServiceProviderModel;
import com.goavega.mporium.R;
import com.goavega.mporium.Services.DashBoardService;
import com.goavega.mporium.Services.DeviceService;
import com.goavega.mporium.Services.ManufacturerService;
import com.goavega.mporium.Services.ServiceProviderService;
import com.goavega.mporium.db.AddDeviceTable;
import com.goavega.mporium.db.ManufacturerTable;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;


public class AddDeviceActivity extends AppCompatActivity{

    ImageView backArrowImage;
    ApiHelper apiHelper;
    AddDeviceTable addDeviceTable;
    boolean success=false;
    java.sql.Date purchaseDateDb;
    String deviceName,
            purchaseDate,
            manufacturerName,
            serviceProviderName,
            modelNumber,
            serialNumber,
            warrantyPeriod,
            serviceSchedule;
    DashBoardService dashBoardService;
    AutoCompleteTextView  deviceNameText;
    EditText selectPurchaseDate,
            modelNumberText,
            serialNumberText,
            warrantyPeriodText,
            serviceScheduleText,
            manufacturerNameText,
            serviceProviderNameText;
    DeviceService deviceService;
    DeviceDetailModel deviceDetailModel;
    ArrayList<Long> manufacturerIdArray=new ArrayList<Long>();
    ArrayList<String> manufacturerNameArray=new ArrayList<String>();
    ArrayList<Long> serviceProviderIdArray=new ArrayList<Long>();
    Set<String> hs = new HashSet<>();
    Set<String> rs = new HashSet<>();
    ArrayList<String> serviceProviderNameArray=new ArrayList<String>();
    Button addDeviceButton;
    ManufacturerTable manufacturerTable;
    Calendar calendar;
    ApiUrls apiUrls;
    boolean f=false;

    TextView custom_message;
    Toast toast;
    View layout;

    Long manufacturerId,
            serviceProviderId,
            serviceProviderIdFromText,
            manufacturerIdFromText;
    Date datesss;
    TextView cancelText;
    int year,
            month,
            day,
            purchaseMonth,
            purchaseYear,
            purchaseDay;
    String userEmail;
    ManufacturerDetailModel manufacturerDetailModel;
    ServiceProviderModel serviceProviderModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_device);
        dashBoardService=new DashBoardService(AddDeviceActivity.this);
        selectPurchaseDate=(EditText)findViewById(R.id.purchaseDateText);
        manufacturerNameText=(EditText)findViewById(R.id.manufacturerText);
        serviceProviderNameText=(EditText)findViewById(R.id.serviceProviderText);
        modelNumberText = (EditText) findViewById(R.id.modelNumberTexts);
        serialNumberText = (EditText) findViewById(R.id.serialNumberTexts);
        warrantyPeriodText = (EditText) findViewById(R.id.warrantyPeriodTexts);
        serviceScheduleText = (EditText) findViewById(R.id.serviceSchedulerTexts);
        addDeviceButton=(Button)findViewById(R.id.addButton);

        LayoutInflater li = getLayoutInflater();
        layout = li.inflate(R.layout.success_message_display,
                (ViewGroup) findViewById(R.id.message_display));
        toast = new Toast(getApplicationContext());
        toast.setDuration(Toast.LENGTH_SHORT);
        toast.setGravity(Gravity.FILL_HORIZONTAL | Gravity.TOP, 0, 0);
        toast.setView(layout);
        custom_message=(TextView)layout.findViewById(R.id.messageText);

        deviceNameText=(AutoCompleteTextView)findViewById(R.id.addDeviceText);
        cancelText=(TextView)findViewById(R.id.cancelText);
        backArrowImage=(ImageView)findViewById(R.id.backArrow);
        cancelText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        backArrowImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(), DashBoardActivity.class);
                startActivity(intent);
                finish();
            }
        });
        ArrayAdapter<String> deviceAdapter=new ArrayAdapter(this,R.layout.spinner_item,getResources().getStringArray(R.array.devices));
        deviceNameText.setAdapter(deviceAdapter);
        deviceNameText.setThreshold(1);
        apiUrls=new ApiUrls();
        serviceProviderModel=new ServiceProviderModel();
        deviceDetailModel=new DeviceDetailModel();
        apiHelper=new ApiHelper(AddDeviceActivity.this);
        deviceService=new DeviceService(AddDeviceActivity.this);
        manufacturerTable = new ManufacturerTable(AddDeviceActivity.this);
        addDeviceTable = new AddDeviceTable(AddDeviceActivity.this);
        userEmail= SaveUserLogin.getUserName(AddDeviceActivity.this);
        calendar = Calendar.getInstance();
        year = calendar.get(Calendar.YEAR);
        month = calendar.get(Calendar.MONTH);
        day = calendar.get(Calendar.DAY_OF_MONTH);
        manufacturerDetailModel=deviceService.getManufacturerDetails();
        serviceProviderModel=deviceService.getServiceProviderDetails();


        selectPurchaseDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showDialog(999);


            }
        });
        manufacturerNameText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                addManufacturer();
            }
        });
        serviceProviderNameText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                addServiceProvider();
            }
        });
        addDeviceButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                deviceDetailModel.Errors.clear();
                boolean present, f;
                Long userId = SaveUserLogin.getUserId(AddDeviceActivity.this);
                modelNumber = modelNumberText.getText().toString();
                serialNumber = serialNumberText.getText().toString();
                warrantyPeriod = warrantyPeriodText.getText().toString();
                serviceSchedule = serviceScheduleText.getText().toString();
                deviceName = deviceNameText.getText().toString();
                purchaseDate = selectPurchaseDate.getText().toString();
                manufacturerName = manufacturerNameText.getText().toString();
                serviceProviderName = serviceProviderNameText.getText().toString();

                if (!deviceName.equals(""))
                {
                    deviceName=Character.toUpperCase(deviceName.charAt(0)) + deviceName.substring(1);
                }
                if (warrantyPeriod.isEmpty())
                    deviceDetailModel.setWarrantyPeriod(0);
                else
                    deviceDetailModel.setWarrantyPeriod(Integer.parseInt(warrantyPeriodText.getText().toString()));
                if (serviceSchedule.isEmpty())
                    deviceDetailModel.setServiceSchedulePeriod(0);
                else
                    deviceDetailModel.setServiceSchedulePeriod(Integer.parseInt(serviceScheduleText.getText().toString()));
                deviceDetailModel.setUserId(userId);
                deviceDetailModel.setServiceProviderId(serviceProviderId);
                deviceDetailModel.setManufacturerId(manufacturerId);
                deviceDetailModel.setDeviceName(deviceName);
                deviceDetailModel.setPurchaseDate(purchaseDate);
                deviceDetailModel.setPurchaseDateDb(purchaseDateDb);
                deviceDetailModel.setModelNumber(modelNumber);
                deviceDetailModel.setSerialNumber(serialNumber);
                deviceDetailModel.setManufacturerName(manufacturerName);
                deviceDetailModel.setServiceProviderName(serviceProviderName);

                boolean success = deviceService.sendDeviceData(deviceDetailModel);
                if (success) {

                    layout.setBackgroundResource(R.color.lightGreen);
                    custom_message=(TextView)layout.findViewById(R.id.messageText);
                    custom_message.setText(R.string.device_update);
                    toast.show();

                    //Toast.makeText(AddDeviceActivity.this,R.string.device_added,Toast.LENGTH_LONG).show();
                    Intent gotoAddDevice = new Intent(AddDeviceActivity.this, DashboardDisplayActivity.class);
                    startActivity(gotoAddDevice);
                } else {
                    layout.setBackgroundResource(R.color.lightGreen);
                    custom_message=(TextView)layout.findViewById(R.id.messageText);
                    custom_message.setText(deviceDetailModel.getErrors().get(0));
                    toast.show();
                    /*for (int j = 0; j < deviceDetailModel.getErrors().size(); j++) {
                        Toast.makeText(AddDeviceActivity.this, deviceDetailModel.getErrors().get(j), Toast.LENGTH_LONG).show();
                    }*/
                }
            }
        });
    }
    //Date of purchase picker
    @Override
    protected Dialog onCreateDialog(int id) {

        if (id == 999) {

            DatePickerDialog dialog = new DatePickerDialog(this, myDateListener, year, month, day);
            dialog.getDatePicker().setMaxDate(new Date().getTime());
            return dialog;
        }
        return null;
    }
    private DatePickerDialog.OnDateSetListener myDateListener = new DatePickerDialog.OnDateSetListener() {
        @Override
        public void onDateSet(DatePicker arg0, int arg1, int arg2, int arg3) {
            showDate(arg1, arg2 + 1, arg3);
            purchaseMonth = arg2 + 1;
            purchaseDay = arg3;
            purchaseYear = arg1;
        }
    };

    private void showDate(int year, int month, int day) {
        selectPurchaseDate.setText(new StringBuilder().append(day).append("/")
                .append(month).append("/").append(year));
        Calendar cal = Calendar.getInstance();
        cal.set(Calendar.YEAR, year);
        cal.set(Calendar.MONTH, month - 1);
        cal.set(Calendar.DATE, day);
        cal.clear(Calendar.HOUR_OF_DAY);
        cal.clear(Calendar.MINUTE);
        cal.clear(Calendar.SECOND);
        cal.clear(Calendar.MILLISECOND);
        purchaseDate = selectPurchaseDate.getText().toString();
        datesss = cal.getTime();
        purchaseDateDb=new java.sql.Date(datesss.getTime());
        boolean f=false;

    }

    public void addManufacturer()
    {
        final EditText
                mobilNumberText;
        final AutoCompleteTextView nameText,
                cityText;
        callManufacturerName();
        TextView cancelText,
                addManufacturerText;

        final Dialog d = new Dialog(AddDeviceActivity.this);
        d.requestWindowFeature(Window.FEATURE_NO_TITLE);
        d.setContentView(R.layout.add_manufacturer_dialog);
        cancelText = (TextView) d.findViewById(R.id.cancelText);
        addManufacturerText = (TextView) d.findViewById(R.id.addManufacturerText);
        nameText=(AutoCompleteTextView)d.findViewById(R.id.NameText);
        cityText=(AutoCompleteTextView)d.findViewById(R.id.cityText);
        mobilNumberText=(EditText)d.findViewById(R.id.mobileNumberText);
        ArrayAdapter<String> manufacturerNameAdapter=new ArrayAdapter(this,android.R.layout.simple_list_item_1,manufacturerNameArray);
        nameText.setAdapter(manufacturerNameAdapter);

        ArrayAdapter manufacturerCityAdapter=new ArrayAdapter(this,android.R.layout.simple_list_item_1,getResources().getStringArray(R.array.cities));
        cityText.setAdapter(manufacturerCityAdapter);
        d.show();
        nameText.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                manufacturerIdFromText = manufacturerIdArray.get(position);
            }
        });

        addManufacturerText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int f=0;
                if (nameText.getText().toString().isEmpty()) {
                    nameText.setHint("Name cannot be blank");
                    nameText.setHintTextColor(getResources().getColor(R.color.red));
                    f=1;
                }
                if(!nameText.getText().toString().isEmpty()) {

                    Boolean isSaved = false;
                    ManufacturerDetailModel manufacturerDetailModel;
                    ManufacturerService manufacturerService;
                    manufacturerDetailModel = new ManufacturerDetailModel();
                    manufacturerService = new ManufacturerService(getApplicationContext());
                    manufacturerDetailModel.setName(nameText.getText().toString().toUpperCase());
                    manufacturerDetailModel.setCity(cityText.getText().toString());
                   // manufacturerDetailModel.setPhoneNumber(mobilNumberText.getText().toString());
                    isSaved = manufacturerService.sendManufacturerData(manufacturerDetailModel);
                    if (isSaved) {
                        manufacturerId = manufacturerDetailModel.getManufacturerId();
                        manufacturerNameText.setText(manufacturerDetailModel.getName());

                        //Toast.makeText(getApplicationContext(), R.string.success, Toast.LENGTH_LONG).show();
                    } else {
                        for (int i = 0; i < manufacturerDetailModel.getErrors().size(); i++) {
                              //Toast.makeText(getApplicationContext(), manufacturerDetailModel.getErrors().get(i), Toast.LENGTH_LONG).show();
                        }
                    }
                }
                else
                {
                    manufacturerNameText.setText(nameText.getText().toString());
                    manufacturerId=manufacturerIdFromText;
                }
               if(f==0)
                    d.dismiss();

            }

        });

        cancelText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                d.dismiss();
            }
        });
    }

    public void addServiceProvider()
    {
        final EditText

                mobilNumberText;
        final AutoCompleteTextView nameText,cityText;
        TextView cancelText,
                addServiceProviderText;
        callServiceProvider();
        final Dialog d = new Dialog(AddDeviceActivity.this);
        d.requestWindowFeature(Window.FEATURE_NO_TITLE);
        d.setContentView(R.layout.add_service_provider_dialog);
        cancelText = (TextView) d.findViewById(R.id.cancelText);
        addServiceProviderText = (TextView) d.findViewById(R.id.addManufacturerText);
        nameText=(AutoCompleteTextView)d.findViewById(R.id.NameText);
        cityText=(AutoCompleteTextView)d.findViewById(R.id.cityText);
        mobilNumberText=(EditText)d.findViewById(R.id.mobileNumberText);
        ArrayAdapter<String> serviceProviderAdapter=new ArrayAdapter(this,android.R.layout.simple_list_item_1,serviceProviderNameArray);
        nameText.setAdapter(serviceProviderAdapter);
        ArrayAdapter serviceProviderCityAdapter=new ArrayAdapter(this,android.R.layout.simple_list_item_1,getResources().getStringArray(R.array.cities));
        cityText.setAdapter(serviceProviderCityAdapter);
        d.show();
        nameText.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                //serviceProviderIdFromText = serviceProviderIdArray.get(position);

            }
        });
        addServiceProviderText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int f=0,g=0;
                //if (serviceProviderIdFromText == null) {
                    Boolean isSaved = false;

                    if (nameText.getText().toString().isEmpty()) {
                        nameText.setHint("Name cannot be blank");
                        nameText.setHintTextColor(getResources().getColor(R.color.red));
                        f=1;
                    }
                    int length=mobilNumberText.getText().length();
                    if (!mobilNumberText.getText().toString().isEmpty()) {
                        if (length < 10) {
                            mobilNumberText.setText("");
                            mobilNumberText.setHint("mobile number should be 10 digit");
                            mobilNumberText.setHintTextColor(getResources().getColor(R.color.red));
                            g=1;
                        }
                    }
                    ServiceProviderModel serviceProviderModel;
                    ServiceProviderService serviceProviderService;
                    serviceProviderModel = new ServiceProviderModel();
                    serviceProviderService = new ServiceProviderService(getApplicationContext());
                    serviceProviderModel.setName(nameText.getText().toString().toUpperCase());
                    serviceProviderModel.setCity(cityText.getText().toString());
                    serviceProviderModel.setPhoneNumber(mobilNumberText.getText().toString());
                    isSaved = serviceProviderService.sendServiceProviderData(serviceProviderModel);

                    if (isSaved) {
                        serviceProviderId = serviceProviderModel.getServiceProviderId();
                        serviceProviderNameText.setText(serviceProviderModel.getName());
                        } else {
                        for (int i = 0; i < serviceProviderModel.getErrors().size(); i++) {
                            //Toast.makeText(getApplicationContext(), serviceProviderModel.getErrors().get(i), Toast.LENGTH_LONG).show();
                        }
                    }
               // }
                 /*else {
                    serviceProviderNameText.setText(nameText.getText().toString());
                    serviceProviderId = serviceProviderIdFromText;
                }*/
                if(f==0&&g==0)
                    d.dismiss();
            }
        });

        cancelText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                d.dismiss();
            }
        });
    }

    public void callManufacturerName() {
        String Manu = apiHelper.readAllUserDetails(getString(R.string.url)+apiUrls.getManufacturerUrl);
        JSONArray manufacturerJsonArray=null;
        JSONObject manufacturerJsonObject=null;
        try {
            manufacturerJsonObject = new JSONObject(Manu);
            manufacturerJsonArray = manufacturerJsonObject.getJSONArray("manufacturerDetails");
            for (int i = 0; i < manufacturerJsonArray.length(); i++) {
                manufacturerJsonObject = manufacturerJsonArray.getJSONObject(i);
                manufacturerIdArray.add(manufacturerJsonObject.getLong("id"));
                manufacturerNameArray.add(manufacturerJsonObject.getString("manufacturerName"));
            }
            rs.addAll(manufacturerNameArray);
            manufacturerNameArray.clear();
            manufacturerNameArray.addAll(rs);

        }
        catch (JSONException e) {
            e.printStackTrace();
        }

    }
    public void callServiceProvider() {
        JSONArray serviceProviderJsonArray=null;
        JSONObject serviceProviderObject=null;
        try {
            String serviceProviderDetails =apiHelper.readAllUserDetails(getString(R.string.url)+apiUrls.getServiceProviderUrl);
            serviceProviderObject = new JSONObject(serviceProviderDetails);
            serviceProviderJsonArray = serviceProviderObject.getJSONArray("serviceProviderDetails");
            for (int i = 0; i < serviceProviderJsonArray.length(); i++) {
                serviceProviderObject = serviceProviderJsonArray.getJSONObject(i);
                serviceProviderIdArray.add(serviceProviderObject.getLong("serviceProviderId"));
                serviceProviderNameArray.add(serviceProviderObject.getString("serviceProviderName"));
            }
            hs.addAll(serviceProviderNameArray);
            serviceProviderNameArray.clear();
            serviceProviderNameArray.addAll(hs);


        } catch (JSONException e) {
            e.printStackTrace();
        }
    }}
