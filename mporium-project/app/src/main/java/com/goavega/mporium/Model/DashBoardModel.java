package com.goavega.mporium.Model;

import com.goavega.mporium.Classes.BaseModel;
import com.goavega.mporium.db.MyDbOpenHelper;

import java.sql.Date;

/**
 * Created by RAKESH on 2/1/2016.
 */
public class DashBoardModel extends BaseModel{
    String deviceName;
    Integer serviceRemainingDays;
    String ServiceScheduleDate;
    Long userId;
    String purchaseDate;
    String manufacturerName;
    String serviceScheduleExpire;
    String warrantyPeriodExpire;
    Integer warrantyRemainingDays;
    String color;
    Long deviceId;
    public String getServiceScheduleDate() {
        return ServiceScheduleDate;
    }

    public void setServiceScheduleDate(String serviceScheduleDate) {
        ServiceScheduleDate = serviceScheduleDate;
    }

    public Integer getWarrantyRemainingDays() {
        return warrantyRemainingDays;
    }

    public void setWarrantyRemainingDays(Integer warrantyRemainingDays) {
        this.warrantyRemainingDays = warrantyRemainingDays;
    }


    public Integer getServiceRemainingDays() {
        return serviceRemainingDays;
    }

    public void setServiceRemainingDays(Integer serviceRemainingDays) {
        this.serviceRemainingDays = serviceRemainingDays;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public Integer getServiceSchedulePeriod() {
        return serviceSchedulePeriod;
    }

    public void setServiceSchedulePeriod(Integer serviceSchedulePeriod) {
        this.serviceSchedulePeriod = serviceSchedulePeriod;
    }

    Integer serviceSchedulePeriod;
    public void setDeviceId(Long deviceId) {
        this.deviceId = deviceId;
    }

    public Long getDeviceId() {
        return deviceId;
    }

    public Integer getWarrantyPeriods() {
        return warrantyPeriods;
    }

    public void setWarrantyPeriods(Integer warrantyPeriods) {
        this.warrantyPeriods = warrantyPeriods;
    }

    Integer warrantyPeriods;

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }



    public String getWarrantyPeriod() {
        return warrantyPeriod;
    }

    public void setWarrantyPeriod(String warrantyPeriod) {
        this.warrantyPeriod = warrantyPeriod;
    }
    String warrantyPeriod;

    public String getServiceScheduleExpire() {
        return serviceScheduleExpire;
    }

    public void setServiceScheduleExpire(String serviceScheduleExpire) {
        this.serviceScheduleExpire = serviceScheduleExpire;
    }

    public String getWarrantyPeriodExpire() {
        return warrantyPeriodExpire;
    }

    public void setWarrantyPeriodExpire(String warrantyPeriodExpire) {
        this.warrantyPeriodExpire = warrantyPeriodExpire;
    }

    public String getDeviceName() {
        return deviceName;
    }

    public void setDeviceName(String deviceName) {
        this.deviceName = deviceName;
    }

    public String getPurchaseDate() {
        return purchaseDate;
    }

    public void setPurchaseDate(String purchaseDate) {
        this.purchaseDate = purchaseDate;
    }

    public String getManufacturerName() {
        return manufacturerName;
    }

    public void setManufacturerName(String manufacturerName) {
        this.manufacturerName = manufacturerName;
    }

    }
